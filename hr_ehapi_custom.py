from openerp.osv import fields, osv
import time
import openerp
from openerp import api
from openerp.tools.translate import _
import datetime
from datetime import timedelta, date
# # import HTML
# import xlrd
import os


class hr_custom_fields(osv.osv):
	def seq_id(self, cr, uid, ids, context=None):
		myobj = self.pool.get('hr.employee')
		ids = myobj.search(cr, uid, [])
		pid = len(ids) + 1
		eid = str(pid)
		return eid
	_inherit = "hr.employee"
	_columns = {
		'sl_no' : fields.char('Sr. no.'),
		'emp_id': fields.char('SAP Emp ID'),
		'passport_issue_date' : fields.date('PP Issue date'),
		'passport_expiry': fields.date('PP Exp. Date'),
		'passport_issued_at' : fields.many2one('passport.issuedat','PP Issued at'),
		'passport_status': fields.selection([('company', 'Company'), ('employee', 'Employee')],
									 'Passport Status'),

		'otherid_expiry': fields.date('National ID Expiry'),
		'lc_no': fields.char('LC No.'),
		'lc_personal_no': fields.char('LC Personal No.'),
		'lc_expiry': fields.date('LC Expiry Date'),

		'doj': fields.date('Date of Joining'),
		'visatype': fields.selection([('cmp_visa', 'Company Visa'), ('tourist_visa', 'Tourist Visa')],
									 'Visa Type'),
		'driving_licn': fields.boolean('Driving Licence'),
		'drvng_no': fields.char('Driving Licence No'),
		"driving_incharge" : fields.many2one('hr.employee','Incharge'),

		'driving_rew_status': fields.selection([('active','Active'),('np1','Notification Period1'),('np2','Notification Period2'),('gp','Grace Period'),('ur','Under Renewal'),('rd','Renewed'),('ex','Expired')],'Status'),


		'visano': fields.char('Residence Visa no.'),
		'visa_company' : fields.many2one('res.company','VISA Company'),
		'cemex_comp' : fields.many2one('res.company','Cemex Company'),
		'visastart': fields.date('Visa Issue Date'),
		'visaend': fields.date('Visa Expiry Date'),
		'visa_incharge_id' : fields.many2one('hr.employee','Incharge 1'),
		'visa_incharge_id_2' : fields.many2one('hr.employee','Incharge 2'),

		'visa_status'  :fields.selection([('active','Active'),('np1','Notification Period1'),('np2','Notification Period2'),('gp','Grace Period'),('ur','Under Renewal'),('rd','Renewed'),('ex','Expired')],'Status'),


		'medical_insurance_type' : fields.many2one('insurance.policy','Medical Insurance'),
		'medicalins': fields.char('Medical Insurance Number',invisible=True),
		'medicalstart': fields.date('Start Date'),
		'medicalend': fields.date('Expiry Date'),
		'medical_incharge_id' : fields.many2one('hr.employee','Incharge'),
		'medical_status'  :fields.selection([('active','Active'),('np1','Notification Period1'),('np2','Notification Period2'),('gp','Grace Period'),('ur','Under Renewal'),('rd','Renewed'),('ex','Expired')],'Status'),


		'upload_type'  : fields.boolean('Upload Document?'),
		'upload_image_1' : fields.binary('Document Upload'),
		'upload_pdf_1'  : fields.binary('Document Upload Pdf'),
		'add_1' : fields.boolean('Add More'),
		'upload_image_2' : fields.binary('Document Upload'),
		'upload_pdf_2'  : fields.binary('Document Upload Pdf'),

		'tenancy_id'  :fields.many2one('tenancy.company', "Tenancy"),
		'acctype': fields.selection([
			('a1', 'Camp'),
			('a2', 'Labour Camp'),
		], 'Accommodation Type',readonly=True),
		'tencontract': fields.char('Tenancy Contract Number',readonly=True),
		'tenstart': fields.date('Tenancy Contract Start Date',readonly=True),
		'tenend': fields.date('Tenancy Contract End Date',readonly=True),
		"tenancy_incharge" : fields.many2one('hr.employee','Incharge'),

		'tenancy_rew_status'  :fields.selection([('active','Active'),('np1','Notification Period1'),('np2','Notification Period2'),('gp','Grace Period'),('ur','Under Renewal'),('rd','Renewed'),('ex','Expired')],'Status'),


		'labcontract': fields.char('Labour Camp Contract Number'),
		'labstart': fields.date('Labour Camp Contract Start Date'),
		'labend': fields.date('Labour Camp Contract End Date'),

		'emiratesid': fields.char('Emirates ID #'),
		'emiratesend': fields.date('Emirates ID Expiry '),
		"emirateid_incharge" : fields.many2one('hr.employee','Incharge 1'),
		"emirateid_incharge_2" : fields.many2one('hr.employee','Incharge 2'),

		'emirates_rew_status'  :fields.selection([('active','Active'),('np1','Notification Period1'),('np2','Notification Period2'),('gp','Grace Period'),('ur','Under Renewal'),('rd','Renewed'),('ex','Expired')],'Status'),


		'labournumber': fields.char('Labour Card Number'),
		'labourexpiry': fields.date('Labour Card Expiry Date'),
		"labour_incharge" : fields.many2one('hr.employee','Incharge 1'),
		"labour_incharge_2" : fields.many2one('hr.employee','Incharge 2'),

		'labour_rew_status'  :fields.selection([('active','Active'),('np1','Notification Period1'),('np2','Notification Period2'),('gp','Grace Period'),('ur','Under Renewal'),('rd','Renewed'),('ex','Expired')],'Status'),


		'drivingnumber': fields.char('Emp Driving License  No.'),
		'driving_issue_date' : fields.date('Driving License Issue Date'),
		'drivingexpiry': fields.date('Driving License Expiry Date'),

		'certification': fields.boolean('Certification'),
		'namecertification': fields.char('Name of Certification'),
		'numbercertification': fields.char('Number of Certification'),
		'scancertification': fields.binary('Scan Copy of Certification'),
		'expirycertification': fields.date('Expiry of Certification'),
		"certification_incharge" : fields.many2one('hr.employee','Incharge'),

		'tenancycmp_id' : fields.many2one('tenancy.company','Tenancy'),

		'certification_rew_status'  :fields.selection([('active','Active'),('np1','Notification Period1'),('np2','Notification Period2'),('gp','Grace Period'),('ur','Under Renewal'),('rd','Renewed'),('ex','Expired')],'Status'),
	   

		'med_ins_id' : fields.one2many('medicalins.docs','emp_med_id','Medical Checkups'),
		'visa_docs_id' : fields.one2many('upload.visa.docs','visa_doc_id','Multi Upload Images'),
		'accommodation_emp_docs_id' : fields.one2many('upload.accom.emp.docs','acc_emp_doc_id','Multi Upload Images'),
		'driving_lic_emp_docs' : fields.one2many('upload.driving.lic','emp_driving_id','Upload Driving Docs'),
		'emirates_id_emp_docs' : fields.one2many('upload.emirates.id','emp_emirates_id','Upload Emirates ID Docs'),
		'labour_id_emp_docs' : fields.one2many('upload.labour.id','emp_labour_id','Upload Labour ID Docs'),
		'certificates_info_ids' : fields.one2many('certificate.emp.details','certificate_emp_id','Certitificates'),
		'profession_visa_id' : fields.many2one('profession.visa','Profession (Visa)'),
		'functional_area' : fields.many2one('functional.area','Functional Area'),
		'religion' : fields.many2one('emp.religion','Religion'),
		'age' : fields.float('Age'),
		'lc_personal_no' : fields.char('LC Personal No.'),
		'lc_issue_date' : fields.date('LC Issue Date'),
		'lc_exp_date' : fields.date('LC Expiry Date'),
		'cemex_company' : fields.many2one('res.company','Cemex Company'),
		'dha_healthy_card_no' : fields.char('DHA Health Card No.'),
		'traffic_code_no' : fields.char('Traffice Code No. '),
		'sap_cost_center' : fields.char('SAP Cost Center'),
		'driving_lic_place_issue' : fields.many2one('lic.issue.place','Driving License Place of Issue'),
		'vehicle_type' : fields.selection([('automatic','Automatic'),('manual','Manual')],'Vehicle Type (Automatic / Manual)'),
		'permitted_vehicle' : fields.char('Permitted Vehicle '),
		'company' : fields.many2one('res.company','Company'),

		'jafza_expiry_date' : fields.date('JAFZA pass expiry date'),
		'jafza_renewed_by' : fields.many2one('hr.employee','Responsible'), 
		'jafza_renewed_date' : fields.date('Jafza Renewed By'), 
		'pass_period' : fields.char('Pass Period'),
		'cost' : fields.float('Cost'),
		'remarks' : fields.text('Remarks'),
	}
	_defaults = {
		'visa_status' : 'active',
		'certification_rew_status' : 'active',
		'labour_rew_status' : 'active',
		'emirates_rew_status' : 'active',
		'tenancy_rew_status' : 'active',
		'medical_status' : 'active',
		'acctype' : 'a1',
		'sl_no' : seq_id,
	}

	@api.onchange('tenancy_id')
	def _get_tenancy_details(self):
		self.tencontract = self.tenancy_id.tenancy_no
		self.tenstart  =self.tenancy_id.start_date
		self.tenend  =self.tenancy_id.end_date
		self.tenancy_incharge  = self.tenancy_id.tenancy_incharge
		self.tenancy_rew_status  = self.tenancy_id.status


	@api.onchange('visastart')
	def _check_changevisa(self):

		if self.visatype == 'cmp_visa':
			dateval = datetime.datetime.strptime(self.visastart, '%Y-%m-%d')
			self.visaend = dateval + timedelta(days=730)

		if self.visatype == 'v2':
			dateval = datetime.datetime.strptime(self.visastart, '%Y-%m-%d')
			self.visaend = dateval + timedelta(days=730)

	@api.onchange('medicalstart')
	def _check_medical_insurance(self):
		if self.medicalstart:
			dateval = datetime.datetime.strptime(self.medicalstart, '%Y-%m-%d')
			self.medicalend = dateval + timedelta(days=365)


	@api.onchange('labstart')
	def _check_labour_date(self):
		if self.labstart:
			dateval = datetime.datetime.strptime(self.labstart, '%Y-%m-%d')
			self.labend = dateval + timedelta(days=365)
	
	@api.onchange('medicalend')
	def _onchange_expirydate(self):
		if self.medicalend and self.medicalstart:
			start_date = datetime.datetime.strptime(self.medicalstart, '%Y-%m-%d')
			end_date = datetime.datetime.strptime(self.medicalend, '%Y-%m-%d')
			if start_date > end_date:
				raise osv.except_osv(_("Error!"), _("Medical Start Date Must be less than Medical expiry Date"))
				return False

	@api.model
	def create(self, vals):
		if 'medicalend' in vals:
			medicalstart = vals['medicalstart']
			if medicalstart:
				start_date = datetime.datetime.strptime(vals['medicalstart'], '%Y-%m-%d')
				end_date = datetime.datetime.strptime(vals['medicalend'], '%Y-%m-%d')
				if start_date > end_date:
					raise osv.except_osv(_("Error!"), _("Medical Start Date Must be less than Medical expiry Date"))
					return None
		new_id = super(hr_custom_fields,self).create(vals)
		return new_id

	@api.onchange('drivingexpiry')
	def _check_driving_status(self):
		if self.drivingexpiry:
			dateval = datetime.datetime.strptime(self.drivingexpiry, '%Y-%m-%d')
			days_gap = (dateval - datetime.datetime.today()).days
			# print days_gap
			if days_gap >= 0:
				if 30 <= days_gap <= 35:
					self.driving_rew_status = 'np1'

				elif 20 <= days_gap <= 30:
					self.driving_rew_status = 'np2'

				elif 0 <= days_gap < 20:
					self.driving_rew_status = 'gp'

				else:
					self.driving_rew_status = 'active'
			else:
				self.driving_rew_status = 'ex'

	@api.onchange('emiratesend', 'labourexpiry')
	def _check_emirates_status(self):
		if self.emiratesend:
			dateval = datetime.datetime.strptime(self.emiratesend, '%Y-%m-%d')
			days_gap = (dateval - datetime.datetime.today()).days
			# print days_gap
			if days_gap >= 0:
				if 30 <= days_gap <= 35:
					self.emirates_rew_status = 'np1'

				elif 20 <= days_gap <= 30:
					self.emirates_rew_status = 'np2'

				elif 0 <= days_gap < 20:
					self.emirates_rew_status = 'gp'

				else:
					self.emirates_rew_status = 'active'
			else:
				self.emirates_rew_status = 'ex'

		if self.labourexpiry:
			dateval = datetime.datetime.strptime(self.labourexpiry, '%Y-%m-%d')
			days_gap = (dateval - datetime.datetime.today()).days
			# print days_gap
			if days_gap >= 0:
				if 30 <= days_gap <= 35:
					self.labour_rew_status = 'np1'

				elif 20 <= days_gap <= 30:
					self.labour_rew_status = 'np2'

				elif 0 <= days_gap < 20:
					self.labour_rew_status = 'gp'

				else:
					self.labour_rew_status = 'active'
			else:
				self.labour_rew_status = 'ex'

	# @api.multi
	# def write(self,vals):
	#     if 'medicalend' in vals:
	#         if self.medicalstart:
	#             start_date = datetime.datetime.strptime(self.medicalstart, '%Y-%m-%d')
	#             end_date = datetime.datetime.strptime(vals['medicalend'], '%Y-%m-%d')
	#             if start_date > end_date:
	#                 raise osv.except_osv(_("Error!"), _("Medical Start Date Must be less than Medical expiry Date"))
	#                 return None

	#     if 'visaend' in vals:
	#         if self.visastart:
	#             start_date = datetime.datetime.strptime(self.visastart, '%Y-%m-%d')
	#             end_date = datetime.datetime.strptime(vals['visaend'], '%Y-%m-%d')
	#             if start_date > end_date:
	#                 raise osv.except_osv(_("Error!"), _("Visa Start Date Must be less than Visa expiry Date"))
	#                 return None
	#         new_id = super(hr_custom_fields,self).write(vals)
	#         return new_id

	# visa expiry grear than start date logic
	
	@api.onchange('visaend')
	def _onchange_visaexpirydate(self):
		if self.visastart and self.visaend:
			start_date = datetime.datetime.strptime(self.visastart, '%Y-%m-%d')
			end_date = datetime.datetime.strptime(self.visaend, '%Y-%m-%d')
			if start_date > end_date:
				raise osv.except_osv(_("Error!"), _("Visa Start Date Must be less than Visa expiry Date"))
				return False

		if self.visaend:
			dateval = datetime.datetime.strptime(self.visaend, '%Y-%m-%d')
			days_gap = (dateval - datetime.datetime.today()).days
			# print days_gap
			if days_gap >= 0:
				if 30 <= days_gap <= 35:
					self.visa_status = 'np1'

				elif 20 <= days_gap <= 30:
					self.visa_status = 'np2'

				elif 0 <= days_gap < 20:
					self.visa_status = 'gp'

				else:
					self.visa_status = 'active'
			else:
				self.visa_status = 'ex'

	def notify_employee_incharge(self, cr, uid, context=None):
		'''function to send reminders to all incharges'''

		template = self.pool.get('ir.model.data').get_object(cr, uid, 'document_management8', 'notify_employee_incharge')
		exp_date_limit = (datetime.datetime.today() + timedelta(days = 15)).date()
		# Checking Visa Docs
		visa_docs = self.search(cr,uid,[('visaend','>=' ,datetime.datetime.today()),('visaend','<=' ,exp_date_limit)])
		# print visa_docs
		incharges = []
		if visa_docs:
			incharge_data = self.pool.get('hr.employee').read(cr,uid,visa_docs,['visa_incharge_id'])
			try:
				for i in incharge_data:
					to_emails = []
					table_data = HTML.Table(header_row=['Employee Name','SAP ID','Visa No','Visa Expiry Date'])
					if 'visa_incharge_id' in i: # Checking whether incharge is there or not
						if i['visa_incharge_id']:
							if i['visa_incharge_id'][0] not in incharges: # Restricting multiple time notifications
								incharges.append(i['visa_incharge_id'][0])
								incharge_wise_permits = self.search(cr, uid, [('visa_incharge_id','=',i['visa_incharge_id'][0]),('id','in',visa_docs)])
								for j in self.browse(cr,uid,incharge_wise_permits):
									table_data.rows.append([str(j.name), str(j.emp_id), str(j.visano), str(j.visaend)]) # summarizing data
									to_emails.append(str(j.visa_incharge_id.work_email)) # appending to addresses
									if j.visa_incharge_id_2:
										to_emails.append(str(j.visa_incharge_id_2.work_email))
									# to_emails.append(str(i.permit_incharge.parent_id.work_email)) # appending manager addresses
								
								to_emails = list(set(to_emails))
								# print to_emails
								# print table_data
								if to_emails:
									for email in to_emails:
										email_template_obj = self.pool.get('email.template')
										values = email_template_obj.generate_email_batch(cr, uid, template.id, [uid], context=context)
										values['subject'] = 'Reminder to Renew Employee Visa Documents' 
										values['email_to'] = email
										values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following employee visa documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>"%table_data
										values['body'] = 'body_html',
										values['res_id'] = False
										mail_mail_obj = self.pool.get('mail.mail')
										x = uid
										values[str(x)] = values.pop(uid)
										msg_id = mail_mail_obj.create(cr,uid,values)
										# if msg_id:
										mail_mail_obj.send(cr, uid, [msg_id], context=context) #To Send mail
			except Exception as e:
				pass
		
		# Checking Driving Docs
		driving_docs = self.search(cr,uid,[('drivingexpiry','>=' ,datetime.datetime.today()),('drivingexpiry','<=' ,exp_date_limit)])
		# print driving_docs
		incharges = []
		if driving_docs:
			incharge_data = self.pool.get('hr.employee').read(cr,uid,driving_docs,['driving_incharge'])
			try:
				for i in incharge_data:
					to_emails = []
					table_data = HTML.Table(header_row=['Employee Name','SAP ID', 'Driving Licence No.' ,'Driving Docs Expiry Date'])
					if 'driving_incharge' in i: # Checking whether incharge is there or not
						if i['driving_incharge']:
							if i['driving_incharge'][0] not in incharges: # Restricting multiple time notifications
								incharges.append(i['driving_incharge'][0])
								incharge_wise_permits = self.search(cr, uid, [('driving_incharge','=',i['driving_incharge'][0]),('id','in',driving_docs)])
								for j in self.browse(cr,uid,incharge_wise_permits):
									table_data.rows.append([str(j.name),str(j.emp_id),str(j.drivingnumber),str(j.drivingexpiry)]) # summarizing data
									to_emails.append(str(j.driving_incharge.work_email)) # appending to addresses
									# to_emails.append(str(i.permit_incharge.parent_id.work_email)) # appending manager addresses
								# print to_emails
								# print table_data
								to_emails = list(set(to_emails))
								for email in to_emails:
									# if to_emails:
									email_template_obj = self.pool.get('email.template')
									values = email_template_obj.generate_email_batch(cr, uid, template.id, [uid], context=context)
									values['subject'] = 'Reminder to Renew Employee Driving Documents' 
									values['email_to'] = email
									values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following employee driving documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>"%table_data
									values['body'] = 'body_html',
									values['res_id'] = False
									mail_mail_obj = self.pool.get('mail.mail')
									x = uid
									values[str(x)] = values.pop(uid)
									msg_id = mail_mail_obj.create(cr,uid,values)
									# if msg_id:
									mail_mail_obj.send(cr, uid, [msg_id], context=context) #To Send mail
			except Exception as e:
				pass

		# # Checking Emirates Docs
		emirates_docs = self.search(cr,uid,[('emiratesend','>=' ,datetime.datetime.today()),('emiratesend','<=' ,exp_date_limit)])
		# print emirates_docs
		incharges = []
		if emirates_docs:
			incharge_data = self.pool.get('hr.employee').read(cr,uid,emirates_docs,['emirateid_incharge'])
			try:
				for i in incharge_data:
					to_emails = []
					table_data = HTML.Table(header_row=['Employee Name','SAP ID','Emirates ID', 'Emirates ID Expiry Date'])
					if 'emirateid_incharge' in i: # Checking whether incharge is there or not
						if i['emirateid_incharge']:
							if i['emirateid_incharge'][0] not in incharges: # Restricting multiple time notifications
								incharges.append(i['emirateid_incharge'][0])
								incharge_wise_permits = self.search(cr, uid, [('emirateid_incharge','=',i['emirateid_incharge'][0]),('id','in',emirates_docs)])
								for j in self.browse(cr,uid,incharge_wise_permits):
									table_data.rows.append([str(j.name),str(j.emp_id),str(j.emiratesid),str(j.emiratesend)]) # summarizing data
									to_emails.append(str(j.emirateid_incharge.work_email)) # appending to addresses
									if j.emirateid_incharge_2:
										to_emails.append(str(j.emirateid_incharge_2.work_email))
									# to_emails.append(str(i.permit_incharge.parent_id.work_email)) # appending manager addresses
								to_emails = list(set(to_emails))
								if to_emails:
									for email in to_emails:
										email_template_obj = self.pool.get('email.template')
										values = email_template_obj.generate_email_batch(cr, uid, template.id, [uid], context=context)
										values['subject'] = 'Reminder to Renew Employee Emirates ID' 
										values['email_to'] = email
										values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following employee emirates id documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>"%table_data
										values['body'] = 'body_html',
										values['res_id'] = False
										mail_mail_obj = self.pool.get('mail.mail')
										x = uid
										values[str(x)] = values.pop(uid)
										msg_id = mail_mail_obj.create(cr,uid,values)
										# if msg_id:
										mail_mail_obj.send(cr, uid, [msg_id], context=context) #To Send mail
			except Exception as e:
				pass

		# # Checking Labour Docs
		labour_docs = self.search(cr,uid,[('labourexpiry','>=' ,datetime.datetime.today()),('labourexpiry','<=' ,exp_date_limit)])
		# print labour_docs
		incharges = []
		if labour_docs:
			incharge_data = self.pool.get('hr.employee').read(cr,uid,labour_docs,['labour_incharge'])
			try:
				for i in incharge_data:
					to_emails = []
					table_data = HTML.Table(header_row=['Employee Name','SAP ID', 'LC No.', 'Labour Card Expiry Date'])
					if 'labour_incharge' in i: # Checking whether incharge is there or not
						if i['labour_incharge']:
							if i['labour_incharge'][0] not in incharges: # Restricting multiple time notifications
								incharges.append(i['labour_incharge'][0])
								incharge_wise_permits = self.search(cr, uid, [('labour_incharge','=',i['labour_incharge'][0]),('id','in',labour_docs)])
								for j in self.browse(cr,uid,incharge_wise_permits):
									table_data.rows.append([str(j.name),str(j.emp_id),str(j.lc_no),str(j.labourexpiry)]) # summarizing data
									to_emails.append(str(j.labour_incharge.work_email)) # appending to addresses
									if j.labour_incharge_2:
										to_emails.append(str(j.labour_incharge_2.work_email))
									# to_emails.append(str(i.permit_incharge.parent_id.work_email)) # appending manager addresses
								# print to_emails
								# print table_data
								to_emails = list(set(to_emails))
								if to_emails:
									for email in to_emails:
										email_template_obj = self.pool.get('email.template')
										values = email_template_obj.generate_email_batch(cr, uid, template.id, [uid], context=context)
										values['subject'] = 'Reminder to Renew Employee Labour Card Documents' 
										values['email_to'] = email
										values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following employee labour card documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>"%table_data
										values['body'] = 'body_html',
										values['res_id'] = False
										mail_mail_obj = self.pool.get('mail.mail')
										x = uid
										values[str(x)] = values.pop(uid)
										msg_id = mail_mail_obj.create(cr,uid,values)
										# if msg_id:
										mail_mail_obj.send(cr, uid, [msg_id], context=context) #To Send mail
			except Exception as e:
				pass

		# # Checking jafza passes
		jafza_passes = self.search(cr,uid,[('jafza_expiry_date','>=' ,datetime.datetime.today()),('jafza_expiry_date','<=' ,exp_date_limit)])
		# print labour_docs
		incharges = []
		if jafza_passes:
			incharge_data = self.pool.get('hr.employee').read(cr,uid,jafza_passes,['jafza_renewed_by'])
			try:
				for i in incharge_data:
					to_emails = []
					table_data = HTML.Table(header_row=['Employee Name', 'SAP ID', 'Jafza Expiry Date'])
					if 'jafza_renewed_by' in i: # Checking whether incharge is there or not
						if i['jafza_renewed_by']:
							if i['jafza_renewed_by'][0] not in incharges: # Restricting multiple time notifications
								incharges.append(i['jafza_renewed_by'][0])
								incharge_wise_permits = self.search(cr, uid, [('jafza_renewed_by','=',i['jafza_renewed_by'][0]),('id','in',labour_docs)])
								for j in self.browse(cr,uid,incharge_wise_permits):
									table_data.rows.append([str(j.name),str(j.emp_id),str(j.jafza_expiry_date)]) # summarizing data
									to_emails.append(str(j.jafza_renewed_by.work_email)) # appending to addresses
									# to_emails.append(str(i.permit_incharge.parent_id.work_email)) # appending manager addresses
								to_emails = list(set(to_emails))
								if to_emails:
									for email in to_emails:
										email_template_obj = self.pool.get('email.template')
										values = email_template_obj.generate_email_batch(cr, uid, template.id, [uid], context=context)
										values['subject'] = 'Reminder to Renew Employee Jafza Passes' 
										values['email_to'] = email
										values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following employee jafza passes are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>"%table_data
										values['body'] = 'body_html',
										values['res_id'] = False
										mail_mail_obj = self.pool.get('mail.mail')
										x = uid
										values[str(x)] = values.pop(uid)
										msg_id = mail_mail_obj.create(cr,uid,values)
										# if msg_id:
										mail_mail_obj.send(cr, uid, [msg_id], context=context) #To Send mail
			except Exception as e:
				pass
		return True



	def import_employees(self, cr, uid, ids, context=None):
		current_path = os.path.dirname(os.path.abspath(__file__))
		# book = xlrd.open_workbook(current_path + '/excels/1.1. DATA_ Employees file (Topmix).xls')  # reading data
		# company_id = self.pool.get('res.company').search(cr, uid, [('name', '=ilike', 'Cemex Topmix L L C')])

		# book = xlrd.open_workbook(current_path + '/excels/1.2. DATA_ Employees file (Falcon).xls')  # reading data
		# company_id = self.pool.get('res.company').search(cr, uid, [('name', '=ilike', 'Cemex Falcon L L C')]) # reading data
		
		book = xlrd.open_workbook(current_path + '/excels/1.3. DATA_ Employees file (Supermix).xls')  # reading data
		company_id = self.pool.get('res.company').search(cr, uid, [('name', '=ilike', 'Cemex Supermix L L C')])
		sheet = book.sheet_by_index(0)
		# r = sheet.row(0)
		# c = sheet.col_values(1)
		inv_ref_list = []
		counter = 0
		for r in range(4, sheet.nrows):
			emp_dict = {}
			sap_empid = str(sheet.cell(r, 1).value).split(".")
			sap_empid = sap_empid[0]
			emp_name = sheet.cell(r, 2).value
			emp_name = emp_name.encode('utf-8')
			emp_name = str(emp_name).strip()
			# print emp_name
			profession = sheet.cell(r, 3).value

			profession1 = self.pool.get('hr.job').search(cr, uid, [('name','=ilike',str(profession))])
			if len(profession1) == 0:
				profession2 = self.pool.get('hr.job').create(cr, uid, {'name': profession})
				profession = profession2
			else:
				profession = profession1[0]


			Profession_visa = sheet.cell(r, 4).value

			Profession_visa1 = self.pool.get('profession.visa').search(cr, uid, [('name','=ilike',str(Profession_visa))])
			if not Profession_visa1:
				Profession_visa2 = self.pool.get('profession.visa').create(cr, uid, {'name': str(Profession_visa)})
				Profession_visa = Profession_visa2
			else:
				Profession_visa = Profession_visa1[0]

			department = sheet.cell(r, 5).value
			department_id = self.pool.get('hr.department').search(cr, uid, [('name','=ilike',str(department))])
			if not department_id:
				department_new = self.pool.get('hr.department').create(cr, uid, {'name': department})
				department = department_new
			else:
				department = department_id[0]

			functional_area = sheet.cell(r, 6).value

			functional_area_id = self.pool.get('functional.area').search(cr, uid, [('functional_area_name','=',str(functional_area))])
			if not functional_area_id:
				functional_area_new = self.pool.get('functional.area').create(cr, uid, {'functional_area_name': str(functional_area)})
				functional_area = functional_area_new
			else:
				functional_area = functional_area_id[0]

			category = sheet.cell(r, 7).value
			date_of_joining = sheet.cell(r, 8).value
			if date_of_joining:
				date_of_joining = xlrd.xldate.xldate_as_datetime(date_of_joining, book.datemode)
				date_of_joining = date_of_joining.date()
			else:
				date_of_joining = None
			contact_no = sheet.cell(r, 9).value
			location = sheet.cell(r, 10).value
			nationality = sheet.cell(r, 11).value
			nationality_id = self.pool.get('res.country').search(cr, uid, [('name','=ilike',str(nationality))])
			if not nationality_id:
				nationality_new = self.pool.get('res.country').create(cr, uid, {'name': str(nationality)})
				nationality = nationality_new
			else:
				nationality = nationality_id[0]

			religion = sheet.cell(r, 12).value

			religion_id = self.pool.get('emp.religion').search(cr, uid, [('name','=ilike',str(religion))])
			if not religion_id:
				religion_new = self.pool.get('emp.religion').create(cr, uid, {'name': str(religion)})
				religion = religion_new
			else:
				religion = religion_id[0]

			dob = sheet.cell(r, 13).value
			dob = (xlrd.xldate.xldate_as_datetime(dob, book.datemode)).date()

			days_in_year = 365.2425    
			age = int((date.today() - dob).days / days_in_year)
			# age = sheet.cell(r, 14).value
			passport_status = sheet.cell(r, 15).value
			passport = sheet.cell(r, 16).value
			passport_issue_date = sheet.cell(r, 17).value
			passport_issue_date = (xlrd.xldate.xldate_as_datetime(passport_issue_date, book.datemode)).date()
			passport_expiry = sheet.cell(r, 18).value
			passport_expiry = (xlrd.xldate.xldate_as_datetime(passport_expiry, book.datemode)).date()
			passport_issued_at = sheet.cell(r, 19).value


			pp_isued_id = self.pool.get('passport.issuedat').search(cr, uid, [('passport_issuedat_name','=ilike',str(passport_issued_at))])
			if not pp_isued_id:
				pp_isued_id_new = self.pool.get('passport.issuedat').create(cr, uid, {'passport_issuedat_name': str(passport_issued_at)})
				passport_issued_at = pp_isued_id_new
			else:
				passport_issued_at = pp_isued_id[0]

			visa_no = sheet.cell(r, 20).value
			uid_temp = str(sheet.cell(r, 21).value).split(".")[0]
			visa_issue_date = sheet.cell(r, 22).value
			if visa_issue_date:
				visa_issue_date = (xlrd.xldate.xldate_as_datetime(visa_issue_date, book.datemode)).date()
			else:
				visa_issue_date = None
			visaend = sheet.cell(r, 23).value
			if visaend:
				visaend = (xlrd.xldate.xldate_as_datetime(visaend, book.datemode)).date()
			else:
				visaend = None
			lc_no = str(sheet.cell(r, 24).value).split(".")[0]
			lc_personal_no = repr(sheet.cell(r, 25).value).split(".")[0]
			lc_issue_date = sheet.cell(r, 26).value
			if lc_issue_date:
				lc_issue_date = (xlrd.xldate.xldate_as_datetime(lc_issue_date, book.datemode)).date()
			else:
				lc_issue_date = None
			lc_exp_date = sheet.cell(r, 27).value
			if lc_exp_date:
				lc_exp_date = (xlrd.xldate.xldate_as_datetime(lc_exp_date, book.datemode)).date()
			else:
				lc_exp_date = None
			visa_company = sheet.cell(r, 28).value
			# print visa_company
			visa_company_id = self.pool.get('res.company').search(cr, uid, [('name','ilike',str(visa_company))])
			# print visa_company
			if not visa_company_id:
				visa_company_new = self.pool.get('res.company').create(cr, uid, {'name': str(visa_company)})
				visa_company = visa_company_new
			else:
				visa_company = visa_company_id[0]

			cemex_company = sheet.cell(r, 29).value

			cemex_company_id = self.pool.get('res.company').search(cr, uid, [('name','ilike',str(cemex_company))])
			# print cemex_company
			# if not cemex_company_id:
			# 	cemex_company_new = self.pool.get('res.company').create(cr, uid, {'name': str(cemex_company)})
			# 	cemex_company = cemex_company_new
			if cemex_company_id:
				cemex_company = cemex_company_id[0]

			emiratesid = sheet.cell(r, 30).value
			emiratesend = sheet.cell(r, 31).value
			if emiratesend:
				emiratesend = (xlrd.xldate.xldate_as_datetime(emiratesend, book.datemode)).date()
			else:
				emiratesend = None
			dha_healthy_card_no = str(sheet.cell(r, 32).value).split(".")[0]
			emp_driving_id = sheet.cell(r, 33).value
			driving_lic_place_issue = sheet.cell(r, 34).value

			lic_issued_id = self.pool.get('lic.issue.place').search(cr, uid, [('name','ilike',str(driving_lic_place_issue))])
			# print cemex_company
			if not lic_issued_id:
				lic_issued_id_new = self.pool.get('lic.issue.place').create(cr, uid, {'name': str(driving_lic_place_issue)})
				lic_issued_id = lic_issued_id_new
			else:
				lic_issued_id = lic_issued_id[0]

			driving_issue_date = sheet.cell(r, 35).value
			if driving_issue_date:
				driving_issue_date = (xlrd.xldate.xldate_as_datetime(driving_issue_date, book.datemode)).date()
			else:
				driving_issue_date = None
			drivingexpiry = sheet.cell(r, 36).value
			if drivingexpiry:
				drivingexpiry = (xlrd.xldate.xldate_as_datetime(drivingexpiry, book.datemode)).date()
			else:
				drivingexpiry = None
			traffic_code_no = sheet.cell(r, 37).value
			permitted_vehicle = sheet.cell(r, 38).value
			vehicle_type = sheet.cell(r, 39).value
			sap_cost_center = str(sheet.cell(r, 40).value)
			sap_cost_center = str(sap_cost_center).split('.')[0]

			# email = str(sheet.cell(r, 41).value)
			# work_phone = str(sheet.cell(r, 42).value)
			
			visa_incharge_id_1 = str(sheet.cell(r, 41).value)
			if visa_incharge_id_1:
				visa_incharge1 = self.search(cr, uid, [('name','=ilike', visa_incharge_id_1)])
				if visa_incharge1:
					visa_incharge1 = visa_incharge1[0]
					emp_dict.update({'visa_incharge_id': visa_incharge1})

			visa_incharge_id_2 = str(sheet.cell(r, 42).value)
			if visa_incharge_id_2:
				visa_incharge2 = self.search(cr, uid, [('name','=ilike', visa_incharge_id_2)])
				if visa_incharge2:
					visa_incharge2 = visa_incharge2[0]
					emp_dict.update({'visa_incharge_id_2': visa_incharge2})

			labour_incharge_id_1 = str(sheet.cell(r, 43).value)
			if labour_incharge_id_1:
				labour_incharge1 = self.search(cr, uid, [('name','=ilike', labour_incharge_id_1)])
				if labour_incharge1:
					labour_incharge1 = labour_incharge1[0]
					emp_dict.update({'labour_incharge': labour_incharge1})

			labour_incharge_id_2 = str(sheet.cell(r, 44).value)
			if labour_incharge_id_2:
				labour_incharge2 = self.search(cr, uid, [('name','=ilike', labour_incharge_id_2)])
				if labour_incharge2:
					labour_incharge2 = labour_incharge2[0]
					emp_dict.update({'labour_incharge_2': labour_incharge2})

			emirates_incharge_id_1 = str(sheet.cell(r, 45).value)
			if emirates_incharge_id_1:
				emirates_incharge_id1 = self.search(cr, uid, [('name','=ilike', emirates_incharge_id_1)])
				if emirates_incharge_id1:
					emirates_incharge_id1 = emirates_incharge_id1[0]
					emp_dict.update({'emirateid_incharge': emirates_incharge_id1})

			emirates_incharge_id_2 = str(sheet.cell(r, 46).value)
			if emirates_incharge_id_2:
				emirates_incharge_id2 = self.search(cr, uid, [('name','=ilike', emirates_incharge_id_2)])
				if emirates_incharge_id2:
					emirates_incharge_id2 = emirates_incharge_id2[0]
					emp_dict.update({'emirateid_incharge_2': emirates_incharge_id2})

			driving_incharge = str(sheet.cell(r, 47).value)
			if driving_incharge:
				driving_incharge_id = self.search(cr, uid, [('name','=ilike', driving_incharge)])
				if driving_incharge_id:
					driving_incharge_id = driving_incharge_id[0]
					emp_dict.update({'driving_incharge': driving_incharge_id, 'driving_licn': True})

			emp_dict.update({
				'name': emp_name,
				'emp_id': sap_empid,
				'doj': date_of_joining,
				'mobile_phone': str(contact_no),
				'functional_area': str(functional_area),
				'work_location': str(location),
				'department_id': department,
				'job_id': profession,
				'profession_visa_id': Profession_visa,
				'country_id': nationality,
				'identification_id': uid_temp,
				'passport_status': str(passport_status).strip().lower(),
				'passport_id': passport,
				'passport_issue_date': passport_issue_date,
				'passport_expiry': passport_expiry,
				'passport_issued_at': passport_issued_at,
				'visano': visa_no,
				'visastart': visa_issue_date,
				'visaend': visaend,
				'visatype': 'cmp_visa',
				'lc_no': lc_no,
				'lc_personal_no': lc_personal_no,
				'lc_issue_date': lc_issue_date,
				'lc_exp_date': lc_exp_date,
				'visa_company': visa_company,
				'cemex_company': cemex_company,
				'emiratesid': emiratesid,
				'emiratesend': emiratesend,
				'dha_healthy_card_no':dha_healthy_card_no,
				'drivingnumber': emp_driving_id,
				'driving_lic_place_issue': lic_issued_id,
				'driving_issue_date': driving_issue_date,
				'drivingexpiry': drivingexpiry,
				'traffic_code_no': traffic_code_no,
				'permitted_vehicle': permitted_vehicle,
				'vehicle_type': str(vehicle_type).lower(),
				'sap_cost_center': sap_cost_center,
				'birthday':dob,
				'age': age,
				# 'work_email': email,
				# 'work_phone': work_phone,
				'company_id': company_id[0]

				})

			# print emp_dict

			emp_id = self.search(cr, uid, [('name','=ilike',emp_name)])

			if not emp_id:
				self.create(cr, uid, emp_dict)
			else:
				self.write(cr, uid, emp_id, emp_dict)
			counter+=1
			# print counter
		return True

	def importing_licence_data(self, cr, uid, ids, context=None):
		vals = {}
		current_path = os.path.dirname(os.path.abspath(__file__))
		xl_workbook = xlrd.open_workbook(
			current_path + '/excels/1.4. DATA _ Driving License Info.xlsx')
		sheet_names = xl_workbook.sheet_names()
		xl_sheet = xl_workbook.sheet_by_name(sheet_names[0])
		for row in range(2, xl_sheet.nrows):
			vals = {}
			sap_empid = str(xl_sheet.cell(row, 1).value).split('.')
			sap_empid = sap_empid[0]
			driving_licn = str(xl_sheet.cell(row, 5).value).split('.')[0]
			issued_place = xl_sheet.cell(row, 6).value

			lic_issued_id = self.pool.get('lic.issue.place').search(cr, uid, [('name','ilike',str(issued_place))])
			# print cemex_company
			if not lic_issued_id:
				lic_issued_id_new = self.pool.get('lic.issue.place').create(cr, uid, {'name': str(issued_place)})
				lic_issued_id = lic_issued_id_new
			else:
				lic_issued_id = lic_issued_id[0]

			iss_date = xl_sheet.cell(row, 7).value
			# print iss_date
			if iss_date:
				iss_date = (xlrd.xldate.xldate_as_datetime(iss_date, xl_workbook.datemode)).date()
				# print iss_date
				vals.update({'driving_issue_date': iss_date})
			exp_date = xl_sheet.cell(row, 8).value
			if exp_date:
				exp_date = (xlrd.xldate.xldate_as_datetime(exp_date, xl_workbook.datemode)).date()
				# print exp_date
				vals.update({'drivingexpiry': exp_date})
			traffic_code_no = str(xl_sheet.cell(row, 9).value).split('.')
			traffic_code_no = traffic_code_no[0]
			permitted_vehicle = str(xl_sheet.cell(row, 10).value).split('.')
			permitted_vehicle = permitted_vehicle[0]
			vehicle_type = xl_sheet.cell(row, 11).value
			vals.update({'drivingnumber': driving_licn,
				'driving_lic_place_issue': lic_issued_id,
				'traffic_code_no': traffic_code_no,
				'permitted_vehicle': permitted_vehicle,
				'vehicle_type': str(vehicle_type).lower(),
				})
			# print vals

			# days_gap = (exp_date - datetime.datetime.now().date()).days
			# # print days_gap
			# if days_gap >= 0:
			# 	if 30 <= days_gap <= 35:
			# 		driving_rew_status = 'np1'

			# 	elif 20 <= days_gap <= 30:
			# 		driving_rew_status = 'np2'

			# 	elif 0 <= days_gap < 20:
			# 		driving_rew_status = 'gp'

			# 	else:
			# 		driving_rew_status = 'active'
			# else:
			# 	driving_rew_status = 'ex'

			# vals.update({'driving_rew_status': driving_rew_status})

			emp_id = self.search(cr, uid, [('emp_id','=ilike', sap_empid)])
			# print emp_id
			self.write(cr, uid, emp_id, vals, context=context)
		return True



	def importing_jafza_data(self, cr, uid, ids, context=None):
		vals = {}
		current_path = os.path.dirname(os.path.abspath(__file__))
		xl_workbook = xlrd.open_workbook(
			current_path + '/excels/7. DATA_JAFZA passes.xlsx')
		sheet_names = xl_workbook.sheet_names()
		xl_sheet = xl_workbook.sheet_by_name(sheet_names[0])
		for row in range(2, xl_sheet.nrows):
			vals = {}
			sap_empid = str(xl_sheet.cell(row, 2).value).split('.')
			sap_empid = sap_empid[0]
			exp_date = xl_sheet.cell(row, 8).value
			if exp_date:
				exp_date = (xlrd.xldate.xldate_as_datetime(exp_date, xl_workbook.datemode)).date()
				print exp_date

			ren_date = xl_sheet.cell(row, 9).value
			if ren_date:
				ren_date = (xlrd.xldate.xldate_as_datetime(ren_date, xl_workbook.datemode)).date()
				print ren_date

			pass_period = xl_sheet.cell(row, 10).value
			cost = xl_sheet.cell(row, 11).value
			remarks = xl_sheet.cell(row, 12).value
			responsible = xl_sheet.cell(row, 13).value

			resp = self.search(cr, uid, [('name','=ilike', responsible)])
			emp_id = self.search(cr, uid, [('emp_id','=', sap_empid)])

			vals.update({'jafza_expiry_date': exp_date,
				'jafza_renewed_date': ren_date,
				'remarks': remarks,
				'pass_period': pass_period,
				'cost': float(cost),
				'jafza_renewed_by': resp[0],
				})
			emp_id = self.search(cr, uid, [('emp_id','=', sap_empid)])
			print emp_id
			self.write(cr, uid, emp_id, vals, context=context)
		return True

hr_custom_fields()


class Upload_Medical_Docs(osv.osv):
	_name = 'medicalins.docs'
	_columns = {
		'medical_checkup_name' : fields.char("Check Up Name"),
		'checkup_desc' : fields.text('Description'),
		'emp_med_id' : fields.many2one('hr.employee'),
		# 'upload_image' : fields.binary('Upload Doc(Image)'),
		'upload_pdf' : fields.binary('Upload Check Ups'),
		'filename':fields.char('Filename'),
	}
Upload_Medical_Docs()

class Upload_visa_docs(osv.osv):
	_name='upload.visa.docs'
	_columns = {
		'visa_doc_id' : fields.many2one('hr.employee'),
		# 'upload_image'  : fields.binary('Upload Doc(Image)'),
		'upload_pdf' : fields.binary('Upload'),
		'filename':fields.char('Filename'),
	}
Upload_visa_docs()

class Upload_Emp_Acc_Docs(osv.osv):
	_name = 'upload.accom.emp.docs'
	_columns = { 
		'acc_emp_doc_id' : fields.many2one('hr.employee'),
		# 'upload_image'  : fields.binary('Upload Doc(Image)'),
		'upload_pdf' : fields.binary('Upload'),
		'filename':fields.char('Filename'),
	}
Upload_Emp_Acc_Docs()

class Upload_Emp_driving_Docs(osv.osv):
	_name = 'upload.driving.lic'
	_columns = { 
		'emp_driving_id' : fields.many2one('hr.employee'),
		# 'upload_image'  : fields.binary('Upload Doc(Image)'),
		'upload_pdf' : fields.binary('Upload'),
		'filename':fields.char('Filename'),
	}
Upload_Emp_driving_Docs()

class Upload_Emp_emirates_id_Docs(osv.osv):
	_name = 'upload.emirates.id'
	_columns = { 
		'emp_emirates_id' : fields.many2one('hr.employee'),
		# 'upload_image'  : fields.binary('Upload Doc(Image)'),
		'upload_pdf' : fields.binary('Upload'),
		'filename':fields.char('Filename'),
	}
Upload_Emp_emirates_id_Docs()

class Upload_Emp_labour_id_Docs(osv.osv):
	_name = 'upload.labour.id'
	_columns = { 
		'emp_labour_id' : fields.many2one('hr.employee'),
		# 'upload_image'  : fields.binary('Upload Doc(Image)'),
		'upload_pdf' : fields.binary('Upload'),
		'filename':fields.char('Filename'),
	}

	# def create(self,cr,uid,vals,context=None):
	# 	file_name=vals['filename']
	# 	if file_name[-4:] == ".pdf":
	# 		return super(Attachments,self).create(cr,uid,vals,context=context)
Upload_Emp_labour_id_Docs()



class Certificates_emp_details(osv.osv):
	_name = 'certificate.emp.details'
	_columns = { 
		'certificate_emp_id' : fields.many2one('hr.employee'),
		'certificate_name' : fields.char('Name of the Certificate'),
		'upload_certificate' : fields.binary('Upload Certificate'),
		'expiry_certificate' : fields.date('Expiry of Certifciate'),
		"certification_emp_incharge" : fields.many2one('hr.employee','Incharge'),
		'certification_rew_emp_status'  :fields.selection([('active','Active'),('np1','Notification Period1'),('np2','Notification Period2'),('gp','Grace Period'),('ur','Under Renewal'),('rd','Renewed'),('ex','Expired')],'Status',required=True),
	}


	@api.onchange('expiry_certificate')
	def _check_certificate_status(self):
		if self.expiry_certificate:
			dateval = datetime.datetime.strptime(self.expiry_certificate, '%Y-%m-%d')
			days_gap = (dateval - datetime.datetime.today()).days
			if days_gap >= 0:
				if 30 <= days_gap <= 35:
					self.certification_rew_emp_status = 'np1'

				elif 20 <= days_gap <= 30:
					self.certification_rew_emp_status = 'np2'

				elif 0 <= days_gap < 20:
					self.certification_rew_emp_status = 'gp'

				else:
					self.certification_rew_emp_status = 'active'
			else:
				self.certification_rew_emp_status = 'ex'

Certificates_emp_details()

class emp_health_cards(osv.osv):

	_name = 'emp.health.cards'

	_rec_name = 'employee_id'
	
	_columns = {
	'employee_id': fields.many2one('hr.employee', 'Employee'),
	'location': fields.char('Location'),
	'profession': fields.many2one('hr.job', 'Profession'),
	'exp_date': fields.date('Expiry Date'),
	'health_card_no': fields.char('Occupation Health Card'),
	'location': fields.char('Location'),
	'responsible_1': fields.many2one('hr.employee', 'Responsible 1'),
	'responsible_2': fields.many2one('hr.employee', 'Responsible 2'),
	'card_type': fields.char('Type of Card'),
	'status'  :fields.selection([('active','Active'),('np1','Notification Period1'),('np2','Notification Period2'),('gp','Grace Period'),('ur','Under Renewal'),('rd','Renewed'),('ex','Expired')],'Status'),

	}

	_defaults = {
	'status': 'active',
	}

	def onchange_employee_id(self, cr, uid, ids, employee_id, context=None):
		res = {'value': {}}
		emp_obj = self.pool.get('hr.employee').browse(cr,uid, employee_id)
		res['value'].update({
			'location': emp_obj.work_location,
			'profession': emp_obj.job_id.id,
			})
		return res

	@api.onchange('exp_date')
	def _check_policy_status(self):
		if self.exp_date:
			dateval = datetime.datetime.strptime(self.exp_date, '%Y-%m-%d')
			days_gap = (dateval - datetime.datetime.today()).days
			if days_gap >= 0:
				if 30 <= days_gap <= 35:
					self.status = 'np1'

				elif 20 <= days_gap <= 30:
					self.status = 'np2'

				elif 0 <= days_gap < 20:
					self.status = 'gp'

				else:
					self.status = 'active'
			else:
				self.status = 'ex'

	def notify_health_card_incharge(self, cr, uid, context=None):
		template = self.pool.get('ir.model.data').get_object(
			cr, uid, 'document_management8', 'notify_permits_incharge')

		exp_date_limit = (datetime.datetime.today() +
						  timedelta(days=15)).date()
		# Getting permit Expiry docs
		health_ids = self.search(cr, uid, [(
			'exp_date', '>=', datetime.datetime.today()), ('exp_date', '<=', exp_date_limit)])

		# expired permits
		expired_permits = self.search(cr, uid, [(
			'exp_date', '<', datetime.datetime.today())])

		if expired_permits:
			self.write(cr, uid, expired_permits, {'status':'ex'}) # moving into expired status

		managers = []
		incharge_data = self.read(
			cr, uid, health_ids, ['responsible_1'])
		incharges = []
		for i in incharge_data:
			to_emails = []
			permits = []
			table_data = HTML.Table(
				header_row=['Document Reference', 'Expiry Date'])
			try:
				if 'responsible_1' in i:  # Checking whether incharge is there or not
					# Restricting multiple time notifications
					if i['responsible_1'][0] not in incharges:
						incharges.append(i['responsible_1'][0])
						incharge_wise_permits = self.search(cr, uid, [(
							'responsible_1', '=', i['responsible_1'][0]), ('id', 'in', health_ids)])
						# print incharge_wise_permits
						for j in self.browse(cr, uid, incharge_wise_permits):
							table_data.rows.append(
								[str(j.health_card_no), str(j.exp_date)])  # summarizing data
							# appending to addresses
							to_emails.append(str(j.responsible_1.work_email))
							if j.responsible_2:
								to_emails.append(str(j.responsible_2.work_email))
							# to_emails.append(str(i.permit_incharge.parent_id.work_email))
							# # appending manager addresses
						to_emails = list(set(to_emails))
						if to_emails:
							for email in to_emails:
								email_template_obj = self.pool.get(
									'email.template')
								values = email_template_obj.generate_email_batch(
									cr, uid, template.id, [uid], context=context)
								values['subject'] = 'Reminder to Renew Health Cards'
								values['email_to'] = email
								values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following helath card documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>" % table_data
								values['body'] = 'body_html',
								values['res_id'] = False
								mail_mail_obj = self.pool.get('mail.mail')
								x = uid
								values[str(x)] = values.pop(uid)
								msg_id = mail_mail_obj.create(cr, uid, values)
								# if msg_id:
								mail_mail_obj.send(
									cr, uid, [msg_id], context=context)  # To Send mail
							# print list(set(to_emails))
						# print table_data
						# print to_emails
			except Exception as e:
				pass
		return True


	def importing_health_data(self, cr, uid, ids, context=None):
		vals = {}
		current_path = os.path.dirname(os.path.abspath(__file__))
		xl_workbook = xlrd.open_workbook(
			current_path + '/excels/6. DATA _ Occupational Health Record.xls')
		sheet_names = xl_workbook.sheet_names()
		xl_sheet = xl_workbook.sheet_by_name(sheet_names[0])
		for row in range(2, xl_sheet.nrows):
			vals = {}
			sap_empid = str(xl_sheet.cell(row, 0).value).split('.')
			sap_empid = sap_empid[0]
			emp_id1 = self.pool.get('hr.employee').search(cr, uid, [('emp_id','=ilike', sap_empid)])
			if emp_id1:
				emp_id1 = emp_id1[0]
			location = str(xl_sheet.cell(row, 8).value)
			profession = str(xl_sheet.cell(row, 2).value)

			profession1 = self.pool.get('hr.job').search(cr, uid, [('name','=ilike',str(profession))])
			if len(profession1) == 0:
				profession2 = self.pool.get('hr.job').create(cr, uid, {'name': profession})
				profession = profession2
			else:
				profession = profession1[0]

			health_card_no = str(xl_sheet.cell(row, 12).value)
			card_type = str(xl_sheet.cell(row, 14).value)
			responsible = str(xl_sheet.cell(row, 17).value)

			emp_id2 = self.pool.get('hr.employee').search(cr, uid, [('name','ilike', responsible)])
			if emp_id2:
				emp_id2 = emp_id2[0]
			vals.update({
				'employee_id': emp_id1,
				'responsible_1': emp_id2,
				'location': location,
				'profession': profession,
				'health_card_no': health_card_no,
				'card_type': card_type,
				})
			# print vals

			h_id = self.search(cr, uid, [('health_card_no', '=', health_card_no)])

			if not h_id:
				self.create(cr, uid, vals, context=context)
			else:
				self.write(cr, uid, h_id, vals, context=context)
		return True

class card_types(osv.osv):

	_name = 'card.type'

	_columns = {
	'name': fields.char('Type/Name'),
	}

class Profession_visa(osv.osv):
	_name = "profession.visa"
	_rec_name = 'name'
	_columns = {
		'name' : fields.char('Profession (Visa)',required=True)
	}
Profession_visa()

class Emp_religion(osv.osv):
	_name = 'emp.religion'
	_rec_name = 'name'
	_columns = {
		'name' : fields.char('Religion Name')
	}
Emp_religion()

class Passport_issued(osv.osv):
	_name = 'passport.issuedat'
	_rec_name = 'passport_issuedat_name'
	_columns = {
		'passport_issuedat_name' : fields.char('Passport issued At'),
	}
Passport_issued()

class Lic_issue_place(osv.osv):
	_name = 'lic.issue.place'
	_rec_name = 'name'
	_columns = {
		'name' : fields.char('Driving License Issued Place')
	}
Lic_issue_place()


class doc_res_users(osv.osv):

	_inherit = 'res.users'

class doc_resource_resource(osv.osv):

	_inherit = 'resource.resource'




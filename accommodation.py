
from openerp.osv import fields, osv
import time
import openerp
from openerp.tools.translate import _
from openerp import api

class Accommodation_Company(osv.osv):

	_name = "accommodation.company"
	_rec_name = 'offacctype'
	_columns = {
		'companyres_id' :fields.many2one('res.company'),
		'offaccbool': fields.boolean('Accommodation',invisible=True),
		'offacctype': fields.selection([
			('o1', 'House'),
			('o2', 'Labour Camp'),
		], 'Accommodation Type'),
		'houseadd': fields.char('House Address'),
		'housecontract': fields.char('House Tenancy Contract'),
		'houseend': fields.date('House Tenancy Contract Expiry Date'),
		'labouradd': fields.char('Labour Camp Address'),
		'labourcontract': fields.char('Labour Camp Tenancy Contract'),
		'labourend': fields.date('Labour Camp Tenancy Contract Expiry Date'),
		'incharge'  : fields.many2one('hr.employee','Incharge'),
		'status'  :fields.selection([('active','Active'),('np1','Notification Period1'),('np2','Notification Period2'),('gp','Grace Period'),('ur','Under Renewal'),('rd','Renewed'),('ex','Expired')],'Status',required=True),

		'accommodation_docs_id' : fields.one2many('upload.accommodation.docs','accommodation_doc_id','Accommodation Docs'),
		

	}
	_defaults = {
		'status' : 'active',
		'offaccbool' : True,
	}
	_
	def create(self,cr,uid,vals,context=None):
		# print "valssss",vals
		rec = super(Accommodation_Company,self).create(cr,uid,vals,context=context)
		return rec	
Accommodation_Company()

# class Tenancy_Category(osv.osv):
# 	_name= 'tenancy.category'
# 	_rec_name = 'Category_name'
# 	_columns = {
# 		'Category_name' : fields.char('Category Name'),
# 	}
# Tenancy_Category()


class Upload_accommodation_docs(osv.osv):
	_name='upload.accommodation.docs'
	_columns = {
		'accommodation_doc_id' : fields.many2one('accommodation.company'),
		# 'upload_image'  : fields.binary('Upload Doc(Image)'),
		'upload_pdf' : fields.binary('Upload Doc(Pdf)'),
		'filename': fields.char('File Name'),
	}
Upload_accommodation_docs()


from openerp.osv import fields, osv
import time
import openerp
from openerp.tools.translate import _
from openerp import api
import datetime


class Comapany_Certification(osv.osv):

    _name = "company.certification"
    _rec_name = 'compcertno'
    _columns = {
        'companyres_id': fields.many2one('res.company'),
        'compcertno': fields.char('Certification Number'),
        'compcertend': fields.date('Certification Expiry Date'),
        # 'not_prior_to_expiry' : fields.char('Notification Prior Expiry(in Days)'),
        'incharge': fields.many2one('hr.employee', 'Incharge'),
        'status': fields.selection([('active', 'Active'), ('np1', 'Notification Period1'), ('np2', 'Notification Period2'), ('gp', 'Grace Period'), ('ur', 'Under Renewal'), ('rd', 'Renewed'), ('ex', 'Expiry')], 'Status', required=True),
    }
    _defaults = {
        'status': 'active',
        'offaccbool': True,
    }
    _

    def create(self, cr, uid, vals, context=None):
        rec = super(Comapany_Certification, self).create(
            cr, uid, vals, context=context)
        return rec

    @api.onchange('compcertend')
    def _check_certificate_status(self):
        if self.compcertend:
            dateval = datetime.datetime.strptime(self.compcertend, '%Y-%m-%d')
            days_gap = (dateval - datetime.datetime.today()).days
            # print days_gap
            if days_gap >= 0:
                if 30 <= days_gap <= 35:
                    self.status = 'np1'

                elif 20 <= days_gap <= 30:
                    self.status = 'np2'

                elif 0 <= days_gap < 20:
                    self.status = 'gp'

                else:
                    self.status = 'active'
            else:
                self.status = 'ex'
Comapany_Certification()

from openerp.osv import fields, osv
import time
import openerp
from openerp.tools.translate import _
from openerp import api
from openerp.tools.translate import _
import datetime
from datetime import timedelta
import os
# import xlrd
# import HTML


# noinspection PyUnreachableCode
class company_documents(osv.osv):

	_inherit = "res.company"

	def _count_emp(self, cr, uid, ids, field_name, arg, context=None):
		try:
			res = {}
			for obj in self.browse(cr, uid, ids, context=context):
				emp = self.pool.get('hr.employee').search(
					cr, uid, [('company_id', '=', obj.id)])
				res[obj.id] = len(emp)
			return res
		except Exception as e:
			print e
			pass

	def visafunc(self, cr, uid, ids, context=None):
		myobj = self.pool.get('hr.employee')
		idval = self.search(cr, uid, [])
		empid = len(idval)
		empvalid = str(empid)
		return empvalid

	def _show_permits(self, cr, uid, ids, name, args, context=None):
		res = {}
		company_ids = self.browse(cr, uid, ids)
		for i in company_ids:
			c_ids = self.pool.get('permits.company').search(
				cr, uid, [('company_id', '=', i.id)])
			permits = self.pool.get('permits.company').read(
				cr, uid, c_ids, ['id'], context=None)
			# print 'permits', permits
			if permits:
				temp_ids = []
				for temp in permits:
					temp_ids.append(temp['id'])
					temp_ids = list(set(temp_ids))
				res[i.id] = temp_ids
		return res

	def _show_tenancy_contracts(self, cr, uid, ids, name, args, context=None):
		res = {}
		company_ids = self.browse(cr, uid, ids)
		for i in company_ids:
			c_ids = self.pool.get('tenancy.company').search(
				cr, uid, [('company_id', '=', i.id)])
			tenancy = self.pool.get('tenancy.company').read(
				cr, uid, c_ids, ['id'], context=None)
			# print 'tenancy', tenancy
			if tenancy:
				temp_ids = []
				for temp in tenancy:
					temp_ids.append(temp['id'])
					temp_ids = list(set(temp_ids))
				res[i.id] = temp_ids
		return res

	_columns = {
		'tradeno': fields.char('Trade License Number'),
		'activity': fields.char('Activity'),
		'tradestart': fields.date('Issue Date'),
		'tradeend': fields.date('Expiry Date'),
		"trade_incharge": fields.many2one('hr.employee', 'Responsible'),
		'trade_status': fields.selection(
			[('active', 'Active'), ('np1', 'Notification Period1'), ('np2', 'Notification Period2'),
			 ('gp', 'Grace Period'), ('ur', 'Under Renewal'), ('rd', 'Renewed'), ('ex', 'Expired')], 'Status'),
		'totalno': fields.integer('Total Number of Visa', required=True),
		'officeloc': fields.char('Office Location'),
		'officeten': fields.char('Office Location Tenancy Contract Number', required=True),
		'officestart': fields.date('Tenancy Contract Start Date', required=True),
		'officeend': fields.date('Tenancy Contract Expiry Date', required=True),
		'compcert': fields.boolean('Company Certification Requirement'),
		'compcertno': fields.char('Company Certification Number'),
		'compcertend': fields.date('Company Certification Expiry Date'),
		'offaccbool': fields.boolean('Accommodation'),
		'offacctype': fields.selection([
			('o1', 'House'),
			('o2', 'Labour Camp'),
		], 'Accommodation Type'),
		'houseadd': fields.char('House Address'),
		'housecontract': fields.char('House Tenancy Contract'),
		'houseend': fields.date('House Tenancy Contract Expiry Date'),
		'labouradd': fields.char('Labour Camp Address'),
		'labourcontract': fields.char('Labour Camp Tenancy Contract'),
		'labourend': fields.date('Labour Camp Tenancy Contract Expiry Date'),
		'tenancy_contract_id': fields.one2many('tenancy.company', 'company_id', 'Tenancy Contract'),
		# 'permit_id': fields.one2many('company.permit', 'permitrel_id', 'Permit'),
		'company_permits': fields.function(_show_permits, relation='permits.company', type="many2many",
										   string='Company Permits'),
		'company_tenancy_contracts': fields.function(_show_tenancy_contracts, relation='tenancy.company',
													 type="many2many", string='Company Tenancy Contracts'),
		'vehical_id': fields.one2many('vehicle.detail', 'companyrel_id', 'Vehicle Info'),
		'emp_count': fields.function(_count_emp, method=True, string="No. of Employees", type='integer'),
		'accommodation_id': fields.one2many('accommodation.company', 'companyres_id', 'Accommodation'),
		'certification_id': fields.one2many('company.certification', 'companyres_id', 'Certification'),
		'trade_lic_ids': fields.one2many('upload.trade.lic', 'company_trade_id', 'Trade Documents'),

	}

	_defaults = {
		'totalno': visafunc,

	}

	@api.onchange('tradestart')
	def _get_tradeend(self):
		if self.tradestart:
			dateval = datetime.datetime.strptime(self.tradestart, '%Y-%m-%d')
			self.tradeend = dateval + timedelta(days=365)

	def import_vehicles_data(self, cr, uid, ids, context=None):
		company_obj = self.pool.get('res.company')
		x = []
		ownnership1 = []
		vehicle_obj = self.pool.get('vehicle.detail')
		current_path = os.path.dirname(os.path.abspath(__file__))
		book = xlrd.open_workbook(
			current_path + '/excels/4. DATA_Company Owned Vehicle list.xlsx')  # reading data
		sheet = book.sheet_by_index(0)
		for r in range(2, sheet.nrows):
			cmp_dict = {}
			fleet_number = str(sheet.cell(r, 0).value).split('.')
			fleet_name = str(sheet.cell(r, 1).value).rstrip()
			exp_date = sheet.cell(r, 2).value
			if exp_date:
				exp_date = xlrd.xldate.xldate_as_datetime(
					exp_date, book.datemode)
				exp_date = exp_date.date()
			else:
				exp_date = None
			category = str(sheet.cell(r, 3).value).rstrip()

			make = str(sheet.cell(r, 4).value).rstrip()
			if make:
				make_id = self.pool.get('vehicle.make').search(
					cr, uid, [('name', '=ilike', str(make))])
				if make_id:
					make_id = make_id[0]
				else:
					make_id = self.pool.get('vehicle.make').create(
						cr, uid, {'name': str(make)})

			owner_ship = str(sheet.cell(r, 5).value).rstrip()
			if owner_ship:
				owner_ship_id = self.pool.get('res.company').search(
					cr, uid, [('name', '=ilike', str(owner_ship))])
				if owner_ship_id:
					owner_ship_id = owner_ship_id[0]
				else:
					owner_ship_id = self.pool.get('res.users').create(cr, uid, {'name': str(owner_ship),
																				'login': str(owner_ship)},
																	  context=context)

			vehicle_obj = [[0, False, {'vehicle_reg_to': False, 'owner_ship': owner_ship_id, 'rate': False,
									   'fleet_name': fleet_name, 'year': False, 'reg_no': False, 'category': category,
									   'fuel_type': False, 'vehicle_belongs_type': 'con_owned_vehicle',
									   'fleet_desc': False, 'functional_area': False, 'emp_id': False,
									   'department_id': False, 'agmt_no': False, 'approver': False, 'supplier': False,
															   'sap_cost_center': False, 'employee_name': False, 'fleet_number': fleet_number[0],
															   'sap_emp_id': False, 'vehicle_reg_from': False, 'make': make_id,
															   'exp_date': exp_date, 'model_name': False}]]

			cmp_dict.update({'vehical_id': vehicle_obj})

			cmp_id = company_obj.search(
				cr, uid, [('name', '=ilike', owner_ship)])

			if not cmp_id:
				cmp_dict.update({
					'name': owner_ship,
				})
				company_obj.create(cr, uid, cmp_dict, context=context)
			else:

				company_obj.write(cr, uid, cmp_id, cmp_dict, context=context)

		return True

	# def create(self, cr, uid, vals, context=None):
	# 	print vals

	# return super(company_documents, self).create(cr, uid, vals,
	# context=context)

	# def write(self, cr, uid, ids, vals, context=None):
	# 	print vals

	# 	return super(company_documents, self).write(cr, uid, ids, vals,
	# 	context=context)

	@api.onchange('tradeend')
	def _check_trade_status(self):
		if self.tradeend:
			dateval = datetime.datetime.strptime(self.tradeend, '%Y-%m-%d')
			days_gap = (dateval - datetime.datetime.today()).days
			# print days_gap
			if days_gap >= 0:
				if 30 <= days_gap <= 35:
					self.trade_status = 'np1'

				elif 20 <= days_gap <= 30:
					self.trade_status = 'np2'

				elif 0 <= days_gap < 20:
					self.trade_status = 'gp'

				else:
					self.trade_status = 'active'
			else:
				self.trade_status = 'ex'

	def notify_trade_licence_incharge(self, cr, uid, context=None):
		template = self.pool.get('ir.model.data').get_object(
			cr, uid, 'document_management8', 'notify_permits_incharge')

		exp_date_limit = (datetime.datetime.today() +
						  timedelta(days=15)).date()
		# Getting permit Expiry docs
		trade_ids = self.search(cr, uid, [(
			'tradeend', '>=', datetime.datetime.today()), ('tradeend', '<=', exp_date_limit)])
		managers = []

		# expired contracts docs
		expired_trade_contracts = self.search(cr, uid, [(
			'tradeend', '<', datetime.datetime.today())])

		if expired_trade_contracts:
			self.write(cr, uid, expired_trade_contracts, {
				'trade_status': 'ex'})  # moving into expired status

		incharge_data = self.read(
			cr, uid, trade_ids, ['trade_incharge'])
		incharges = []
		for i in incharge_data:
			to_emails = []
			permits = []
			table_data = HTML.Table(
				header_row=['Company', 'Document Reference', 'Expiry Date'])
			try:
				if 'trade_incharge' in i:  # Checking whether incharge is there or not
					# Restricting multiple time notifications
					if i['trade_incharge'][0] not in incharges:
						incharges.append(i['trade_incharge'][0])
						incharge_wise_permits = self.search(cr, uid, [(
							'trade_incharge', '=', i['trade_incharge'][0]), ('id', 'in', trade_ids)])
						# print incharge_wise_permits
						for j in self.browse(cr, uid, incharge_wise_permits):
							table_data.rows.append(
								[str(j.name), str(j.tradeno), str(j.tradeend)])  # summarizing data
							# appending to addresses
							to_emails.append(str(j.trade_incharge.work_email))
							# to_emails.append(str(i.permit_incharge.parent_id.work_email))
							# # appending manager addresses
						if to_emails:
							email_template_obj = self.pool.get(
								'email.template')
							values = email_template_obj.generate_email_batch(
								cr, uid, template.id, [uid], context=context)
							values['subject'] = 'Reminder to Renew Trade Licence'
							values['email_to'] = to_emails[0]
							values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following trade licences are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>" % table_data
							values['body'] = 'body_html',
							values['res_id'] = False
							mail_mail_obj = self.pool.get('mail.mail')
							x = uid
							values[str(x)] = values.pop(uid)
							msg_id = mail_mail_obj.create(cr, uid, values)
							# if msg_id:
							mail_mail_obj.send(
								cr, uid, [msg_id], context=context)  # To Send mail
			except Exception as e:
				print e
				pass
		return True

	def importing_rented_vehicles_file_data(self, cr, uid, ids, context=None):
		company_obj = self.pool.get('res.company')
		x = []
		vals = {}

		current_path = os.path.dirname(os.path.abspath(__file__))
		book = xlrd.open_workbook(
			current_path + '/excels/5. DATA_Rented - Company Vehicle list.xlsx')
		sheet_names = book.sheet_names()
		xl_sheet = book.sheet_by_name(sheet_names[0])

		# searching existing records for company name "cemex"
		if self.pool.get('res.company').search(cr, uid, [('name', '=', 'Cemex')]):
			name_object = (self.pool.get('res.company').search(
				cr, uid, [('name', '=', 'Cemex')]))[0]
			order_obj = self.pool.get(
				'res.company').browse(cr, uid, name_object)
			name = order_obj.name
		else:
			company_obj.create(cr, uid, {'name': 'Cemex'}, context=context)
			name_object = (self.pool.get('res.company').search(
				cr, uid, [('name', '=', 'Cemex')]))[0]
			order_obj = self.pool.get(
				'res.company').browse(cr, uid, name_object)

		for row in range(1, xl_sheet.nrows):
			cmp_dict = {}
			fuel_type = (xl_sheet.cell(row, 0).value).lower()
			supplier = xl_sheet.cell(row, 1).value
			if supplier:
				supplier_id = self.pool.get('vehicle.supplier').search(
					cr, uid, [('supplier_name', '=ilike', str(supplier))])
				if supplier_id:
					supplier_id = supplier_id[0]
				else:
					supplier_id = self.pool.get('vehicle.supplier').create(
						cr, uid, {'supplier_name': str(supplier)})

			agmt_no = str(xl_sheet.cell(row, 2).value).split('.')
			agmt_no = agmt_no[0]
			emp_id = str(xl_sheet.cell(row, 3).value).split('.')
			emp_id = emp_id[0]
			if emp_id:
				emp_idd = self.pool.get('hr.employee').search(
					cr, uid, [('name', '=', str(emp_id))])
				if emp_idd:
					emp_idd = emp_idd[0]
			sap_emp_id = str(xl_sheet.cell(row, 4).value).split('.')
			sap_emp_id = sap_emp_id[0]
			employee_name = xl_sheet.cell(row, 5).value
			sap_cost_center = str(xl_sheet.cell(row, 6).value).split('.')
			sap_cost_center = sap_cost_center[0]
			functional_area = xl_sheet.cell(row, 7).value
			if functional_area:
				functional_area_id = self.pool.get('functional.area').search(
					cr, uid, [('functional_area_name', '=ilike', str(functional_area))])
				if functional_area_id:
					functional_area_id = functional_area_id[0]
				else:
					functional_area_id = self.pool.get('functional.area').create(
						cr, uid, {'functional_area_name': str(functional_area)})

			department_id = xl_sheet.cell(row, 8).value
			if department_id:
				department_idd = self.pool.get('hr.department').search(
					cr, uid, [('name', '=ilike', str(department_id))])
				if department_idd:
					department_idd = department_idd[0]
				else:
					department_idd = self.pool.get('hr.department').create(
						cr, uid, {'name': str(department_id)})

			reg_no = str(xl_sheet.cell(row, 9).value).split('.')
			reg_no = reg_no[0]
			fleet_desc = xl_sheet.cell(row, 10).value
			model_name = xl_sheet.cell(row, 11).value
			if model_name:
				model_name = int(model_name)
			else:
				model_name = None
			vehicle_type = xl_sheet.cell(row, 12).value

			rate = xl_sheet.cell(row, 13).value

			date_from = xl_sheet.cell(row, 14).value
			if date_from:
				vehicle_reg_from = xlrd.xldate.xldate_as_datetime(
					date_from, book.datemode)
			else:
				vehicle_reg_from = False

			date_to = xl_sheet.cell(row, 15).value
			if date_to:
				vehicle_reg_to = xlrd.xldate.xldate_as_datetime(
					date_to, book.datemode)
			else:
				vehicle_reg_to = False

			if xl_sheet.cell(row, 16).value:
				year = int(xl_sheet.cell(row, 16).value)
			else:
				year = False

			approver = xl_sheet.cell(row, 17).value
			if approver:
				approver_id = self.pool.get('hr.employee').search(
					cr, uid, [('name', '=', str(approver))])
				if approver_id:
					approver_id = approver_id[0]

			responsible = str(xl_sheet.cell(row, 22).value)
			print responsible
			if responsible:
				responsible_per = self.pool.get('hr.employee').search(
					cr, uid, [('name', 'ilike', str(responsible))])
				if responsible_per:
					responsible_per = responsible_per[0]

			rented_vehicles = [[0, False,
							   {
								   'fuel_type': fuel_type,
								   'supplier': supplier_id,
								   'agmt_no': agmt_no,
								   'emp_id': emp_idd,
								   'sap_emp_id': sap_emp_id,
								   'employee_name': employee_name,
								   'sap_cost_center': sap_cost_center,
								   'functional_area': functional_area_id,
								   'department_id': department_idd,
								   'reg_no': reg_no,
								   'fleet_desc': fleet_desc,
								   'model_name': model_name,
								   'rate': rate,
								   'vehicle_reg_from': vehicle_reg_from,
								   'vehicle_reg_to': vehicle_reg_to,
								   'year': year,
								   'approver': approver_id,
								   'responsible_rent': responsible_per,
								   'vehicle_belongs_type': 'rented_vehicle',
							   }]]
			cmp_dict.update({'vehical_id': rented_vehicles})

			cmp_id = company_obj.search(
				cr, uid, [('name', '=ilike', 'Cemex')])

			if not cmp_id:
				cmp_dict.update({
					'name': 'Cemex',
				})
				cmp_id = company_obj.create(cr, uid, cmp_dict, context=context)
			else:
				company_obj.write(cr, uid, cmp_id, cmp_dict, context=context)
		return True

	def company_owned_vehicles_import(self, cr, uid, ids, context=None):
		company_obj = self.pool.get('res.company')
		x = []
		vals = {}

		current_path = os.path.dirname(os.path.abspath(__file__))
		xl_workbook = xlrd.open_workbook(
			current_path + '/excels/4. DATA_Company Owned Vehicle list.xlsx')
		sheet_names = xl_workbook.sheet_names()
		xl_sheet = xl_workbook.sheet_by_name(sheet_names[0])

		

		for row in range(1, xl_sheet.nrows):
			cmp_dict = {}
			owner_ship = xl_sheet.cell(row, 5).value
			# searching existing records for company name "cemex"
			if self.pool.get('res.company').search(cr, uid, [('name', '=', str(owner_ship))]):
				name_object = (self.pool.get('res.company').search(
					cr, uid, [('name', '=', str(owner_ship))]))[0]
				object1 = self.pool.get('res.company').browse(cr, uid, name_object)
				name = object1.name

			else:
				company_obj.create(cr, uid, {'name': str(owner_ship)}, context=context)
				name_object = (self.pool.get('res.company').search(
					cr, uid, [('name', '=', str(owner_ship))]))[0]
				object1 = self.pool.get('res.company').browse(cr, uid, name_object)

			fleet_number = str(xl_sheet.cell(row, 0).value).split('.')
			fleet_number = fleet_number[0]
			fleet_name = xl_sheet.cell(row, 1).value
			exp_date_id = xl_sheet.cell(row, 2).value
			exp_date = xlrd.xldate.xldate_as_datetime(
				exp_date_id, xl_workbook.datemode)

			reg_category = xl_sheet.cell(row, 3).value
			make = xl_sheet.cell(row, 4).value
			if make:
				make_id = self.pool.get('vehicle.make').search(
					cr, uid, [('name', '=ilike', str(make))])
				if make_id:
					make_id = make_id[0]
				else:
					make_id = self.pool.get('vehicle.make').create(
						cr, uid, {'name': str(make), 'login': str(make)})
			if owner_ship:
				owner_ship_id = self.pool.get('res.company').search(
					cr, uid, [('name', '=', str(owner_ship))])
				if owner_ship_id:
					owner_ship_id = owner_ship_id[0]

			responsible = str(xl_sheet.cell(row, 6).value)

			if responsible:
				responsible_per = self.pool.get('hr.employee').search(
					cr, uid, [('name', '=ilike', str(responsible))])
				if responsible_per:
					responsible_per = responsible_per[0]
			vehicle_obj = [[0, False,
							  {
								  'fleet_number': fleet_number,
								  'fleet_name': fleet_name,
								  'exp_date': exp_date,
								  'category': reg_category,
								  'make': make_id,
								  'owner_ship': owner_ship_id,
								  'responsible': responsible_per,
								  'vehicle_belongs_type': 'con_owned_vehicle',
							  }]]
			cmp_dict.update({'vehical_id': vehicle_obj})
			# print cmp_dict
			cmp_id = company_obj.search(
				cr, uid, [('name', '=ilike', owner_ship)])

			if not cmp_id:
				cmp_dict.update({
					'name': owner_ship,
				})
				company_obj.create(cr, uid, cmp_dict, context=context)
			else:

				company_obj.write(cr, uid, cmp_id, cmp_dict, context=context)
		return True


company_documents()


class vehicle_detail(osv.osv):

	_name = "vehicle.detail"

	_columns = {
		'vehicle_belongs_type': fields.selection(
			[('rented_vehicle', 'Rented Vehicle'), ('con_owned_vehicle', 'Company Owned Vehicle')], 'Vehicle Type'),
		'fuel_type': fields.selection([('diesel', 'DIESEL'), ('petrol', 'PETROL')], 'Fuel'),
		'supplier': fields.many2one('vehicle.supplier', 'Supplier'),
		'agmt_no': fields.char('AgmtNo'),
		'emp_id': fields.many2one('hr.employee', 'Emp ID #'),
		'sap_emp_id': fields.char('SAP Emp ID'),
		'employee_name': fields.char('Employee Name'),
		'sap_cost_center': fields.char('SAP COST CENTER'),
		'functional_area': fields.many2one('functional.area', 'Functional Area'),
		'department_id': fields.many2one('hr.department', 'Department'),
		'reg_no': fields.char('RegnNo'),
		'fleet_desc': fields.text('Fleet Description'),
		'model_name': fields.char('Model'),
		'rate': fields.char('Rate'),
		'vehicle_reg_from': fields.date('From'),
		'vehicle_reg_to': fields.date('To'),
		'year': fields.selection([(num, str(num)) for num in range(1900, (datetime.datetime.now().year) + 10)], 'Year'),
		'approver': fields.many2one('hr.employee', 'Approver'),

		'companyrel_id': fields.many2one('res.company'),
		'novehicles': fields.integer('Number of Vehicles'),
		'vehicleins': fields.char('Insurance of Vehicle'),

		'fleet_number': fields.char('Fleet Number'),
		'fleet_name': fields.char('Fleet Name'),
		'exp_date': fields.date(" Registration Exp Date"),
		'category': fields.char("Category"),
		'make': fields.many2one('vehicle.make', 'Make'),
		'owner_ship': fields.many2one('res.company', 'Ownership'),
		'responsible': fields.many2one('hr.employee', 'Responsible'),
		'responsible_rent': fields.many2one('hr.employee', 'Responsible'),
	}
	_defaults = {
		# 'vehicle_belongs_type': 'com_vehicle',
	}


	def notify_vehicle_incharge(self, cr, uid, context=None):
		template = self.pool.get('ir.model.data').get_object(
			cr, uid, 'document_management8', 'notify_employee_incharge')
		exp_date_limit = (datetime.datetime.today() + timedelta(days=15)).date()
		# Checking Medical Docs
		rented_docs = self.search(cr, uid, [(
			'vehicle_reg_to', '>=', datetime.datetime.today()), ('vehicle_reg_to', '<=', exp_date_limit),('vehicle_belongs_type','=','rented_vehicle')])

		# print rented_docs
		# expired medical docs
		expired_medicals = self.search(cr, uid, [(
			'vehicle_reg_to', '<', datetime.datetime.today())])

		# if expired_medicals:
		#     # moving into expired status
		#     self.write(cr, uid, expired_medicals, {'status': 'ex'})

		managers = []
		incharge_data = self.read(cr, uid, rented_docs, ['responsible_rent'])
		incharges = []
		for i in incharge_data:
			to_emails = []
			table_data = HTML.Table(
				header_row=['Agmt No','Reg No', 'Expiry Date'])
			try:
				if 'responsible_rent' in i:  # Checking whether incharge is there or not
					if i['responsible_rent'][0] not in incharges:  # Restricting multiple time notifications
						incharges.append(i['responsible_rent'][0])
						incharge_wise_docs = self.search(
							cr, uid, [('responsible_rent', '=', i['responsible_rent'][0]), ('id', 'in', rented_docs)])
						for j in self.browse(cr, uid, incharge_wise_docs):
							table_data.rows.append(
								[str(j.agmt_no), str(j.reg_no), str(j.vehicle_reg_to)])  # summarizing data
							# appending to addresses
							to_emails.append(str(j.responsible_rent.work_email))
							if j.approver:
								to_emails.append(
									str(j.approver.work_email))
							# to_emails.append(str(i.permit_incharge.parent_id.work_email))
							# # appending manager addresses
						# print to_emails
						to_emails = list(set(to_emails))
						if to_emails:
							for email in to_emails:
								email_template_obj = self.pool.get(
									'email.template')
								values = email_template_obj.generate_email_batch(
									cr, uid, template.id, [uid], context=context)
								values[
									'subject'] = 'Reminder to Renew Rented Vehicle Documents'
								values['email_to'] = email
								values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following rented vehicle documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>" % table_data
								values['body'] = 'body_html',
								values['res_id'] = False
								mail_mail_obj = self.pool.get('mail.mail')
								x = uid
								values[str(x)] = values.pop(uid)
								msg_id = mail_mail_obj.create(cr, uid, values)
								# if msg_id:
								mail_mail_obj.send(
									cr, uid, [msg_id], context=context)  # To Send mail
			except Exception as e:
				print e
				pass
		# print to_emails
		return True


vehicle_detail()


class Upload_Trade_Docs(osv.osv):
	_name = 'upload.trade.lic'
	_columns = {
		'company_trade_id': fields.many2one('res.company'),
		'upload_image': fields.binary('Upload Doc(Image)'),
		'upload_pdf': fields.binary('Upload Doc(Pdf)'),
	}


Upload_Trade_Docs()


class Supplier(osv.osv):
	_name = 'vehicle.supplier'
	_rec_name = 'supplier_name'
	_columns = {
		'supplier_name': fields.char('Supplier Name', required=True)
	}


Supplier()


class Functional_Area(osv.osv):
	_name = 'functional.area'
	_rec_name = 'functional_area_name'
	_columns = {
		'functional_area_name': fields.char('Functional Area Name')
	}


Functional_Area()


class Vehicle_Make(osv.osv):
	_name = 'vehicle.make'
	_rec_name = 'name'
	_columns = {
		'name': fields.char('Make Name', required=True)
	}


Vehicle_Make()

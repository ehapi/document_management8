
from openerp.osv import fields, osv
import time
import openerp
from openerp.tools.translate import _
from openerp import api
import datetime
from datetime import timedelta
# import HTML
from datetime import datetime

class Hr_training(osv.osv):
	def seq_id(self, cr, uid, ids, context=None):
		myobj = self.pool.get('hr.training')
		ids = myobj.search(cr, uid, [])
		pid = len(ids) + 1
		sid = str(pid)
		return sid

	_name = "hr.training"
	_rec_name = 'participant_name'
	_columns = {
		's_no' : fields.char('SL',readonly=True),
		'lmc_course_title' :fields.char('Course Title'),
		'bu' : fields.char('BU'),
		'fees' : fields.char('Fees(AED)'),
		'participant_name' : fields.char('Participant Name'),
		'cemex_id' : fields.char('Cemex ID'),
		'employee_no' : fields.char('Employee No'),
		'grade' : fields.selection([('successful','Successful'),('unsuccessful','UnSuccessful')],'Grade/Score'),
		'start_date' : fields.date('Start Date'),
		'completion_date' : fields.date('Completion Date'),
		'completion_type' : fields.selection([('month','Month'),('days','days')],'Completion'),
		'duration' : fields.char('Duration'),
		'department' : fields.many2one('hr.department',"Department"),
		'profession_id' : fields.many2one('hr.job',"Profession"),
		'category_id' : fields.many2one('training.category','Category'),
		'sub_category_id' : fields.many2one('sub.training.category','Sub Category'),
		'institution_id' : fields.many2one('hr.institution','Institution'),
		'facilitator' : fields.char("Facilitator"),
		'month' : fields.char('Month'),
	}

	_defaults = {
		's_no' : seq_id,
	}
	@api.multi
	@api.onchange('start_date','completion_date')
	def Cal_Duration(self):
		if self.start_date and self.completion_date:
			fmt = '%Y-%m-%d'
			from_date = self.start_date    
			to_date = self.completion_date
			d1 = datetime.strptime(from_date, fmt)
			d2 = datetime.strptime(to_date, fmt)
			if d1>d2:
				print ("here we have to show error message")
				raise osv.except_osv(_('Error'), _("Start Date should be less than Completion date."))
			daysDiff = str((d2-d1).days + 1)
			self.duration  = daysDiff +  " Days"     
			print ("write logic here",daysDiff)
Hr_training()

class Training_Category_main(osv.osv):
	_name = 'training.category'
	_rec_name = 'category_name'
	_columns = {
		'category_name' : fields.char('Category Name',required=True)
	}
Training_Category_main()

class Sub_Training_Category(osv.osv):
	_name = 'sub.training.category'
	_rec_name = 'category_name'
	_columns = {
		'category_name' : fields.char('Sub Category Name',required=True)
	}
Sub_Training_Category()

class Hr_Institution(osv.osv):
	_name = 'hr.institution'
	_rec_name = 'institution_name'
	_columns = {
		'institution_name' : fields.char('Institution Name',required=True)
	}
Hr_Institution()



from openerp.osv import fields, osv
import time
import openerp
from openerp.tools.translate import _
from openerp import api
import datetime
from datetime import timedelta
# import HTML

class Tenancy_Company(osv.osv):

	def Tenancy_id(self, cr, uid, ids, context=None):
		myobj = self.pool.get('tenancy.company')
		ids = myobj.search(cr, uid, [])
		empid = len(ids) + 1
		eid = 'CT00-' + str(empid)
		return eid


	_name = "tenancy.company"
	_rec_name = 'tenancy_no'
	_columns = {
		'tenancy_contract_id' : fields.many2many('res.company'),
		'tenancy_type' : fields.selection([('office','Office'),('emp','Employees')],'Tenancy For',required=True),
		'tenancy_no' :fields.char('Tenancy No',required=True,readonly=True),
		'name_tenancy' : fields.char('Tenancy Reference',required=True),
		'street': fields.char(string="Street", multi='address'),
		'street2': fields.char(string="Street2", multi='address'),
		'zip': fields.char(string="Zip", multi='address'),
		'city': fields.char(string="City", multi='address'),
		'state_id': fields.many2one('res.country.state', string="Fed. State", multi='address'),
		'country_id': fields.many2one('res.country', string="Country", multi='address'),
		'category' : fields.many2one('tenancy.category','Category'),
		'start_date' : fields.date('Start Date',required=True),
		'end_date' : fields.date('End Date',required=True),
		'tenancy_incharge' : fields.many2one('hr.employee','Tenancy Incharge',required=True),
		'tenancy_incharge_2': fields.many2one('hr.employee','Tenancy Incharge 2'),
		'first_reminder' : fields.many2one('reminder.time','First Reminder(Days)',placeholder='30'),
		'company_id' : fields.many2one('res.company','Company'),
		'second_reminder' : fields.many2one('reminder.time','Second Reminder(Days)',placeholder='15'),
		'status'  :fields.selection([('active','Active'),('np1','Notification Period1'),('np2','Notification Period2'),('gp','Grace Period'),('ur','Under Renewal'),('rd','Renewed'),('ex','Expired')],'Status',required=True),
		'tenancy_docs_id' : fields.one2many('upload.tenancy.docs','tenancy_doc_id','Tenancy Contract Docs'),

	}
	_defaults = {
		'status' : 'active',
		'tenancy_no' : Tenancy_id,
	}
	_sql_constraints = [
		('tenancy_no', 'unique(tenancy_no)', 'Tenancy No Should Be Unique'),
	]
	_
	def create(self,cr,uid,vals,context=None):
		# print "valssss",vals
		rec = super(Tenancy_Company,self).create(cr,uid,vals,context=context)
		return rec

	def onchange_state(self, cr, uid, ids, state_id, context=None):
		if state_id:
			return {'value':{'country_id': self.pool.get('res.country.state').browse(cr, uid, state_id, context).country_id.id }}
		return {}

	@api.onchange('start_date')
	def _check_permit_end_date(self):
		if self.start_date:
			dateval = datetime.datetime.strptime(self.start_date, '%Y-%m-%d')
			self.end_date = dateval + timedelta(days = 365)

	@api.onchange('end_date')
	def _check_permit_status(self):
		if self.end_date:
			dateval = datetime.datetime.strptime(self.end_date, '%Y-%m-%d')
			days_gap =  (dateval - datetime.datetime.today()).days
			# print days_gap
			if days_gap >= 0:
				if 30 <= days_gap <= 35:
					self.status = 'np1'

				elif 20 <= days_gap <= 30:
					self.status = 'np2'
				
				elif 0 <= days_gap < 20:
					self.status = 'gp'
				
				else:
					self.status = 'active'
			else:
				self.status = 'ex'


	def notify_tenancy_incharge(self, cr, uid, context=None):
		
		template = self.pool.get('ir.model.data').get_object(cr, uid, 'document_management8', 'notify_tenancy_incharge')

		exp_date_limit = (datetime.datetime.today() + timedelta(days = 15)).date()
		contract_ids = self.search(cr,uid,[('end_date','>=' ,datetime.datetime.today()),('end_date','<=' ,exp_date_limit)])

		# expired contracts docs
		expired_contracts = self.search(cr, uid, [(
			'end_date', '<', datetime.datetime.today())])

		if expired_contracts:
			self.write(cr, uid, expired_contracts, {'status':'ex'}) # moving into expired status

		# Getting Contract Expiry docs
		managers = []
		incharge_data = self.read(cr,uid,contract_ids,['tenancy_incharge'])
		incharges = []
		for i in incharge_data:
			to_emails = []
			permits = []
			table_data = HTML.Table(header_row=['Document Reference', 'Expiry Date'])
			try:
				if 'tenancy_incharge' in i: # Checking whether incharge is there or not
					if i['tenancy_incharge']:
						if i['tenancy_incharge'][0] not in incharges: # Restricting multiple time notifications
							incharges.append(i['tenancy_incharge'][0])
							incharge_wise_permits = self.search(cr, uid, [('tenancy_incharge','=',i['tenancy_incharge'][0]),('id','in',contract_ids)])
							# print incharge_wise_permits
							for j in self.browse(cr,uid,incharge_wise_permits):
								table_data.rows.append([str(j.tenancy_no),str(j.end_date)]) # summarizing data
								to_emails.append(str(j.tenancy_incharge.work_email)) # appending to addresses
								if j.tenancy_incharge_2:
									to_emails.append(str(j.tenancy_incharge_2.work_email))
								# to_emails.append(str(i.permit_incharge.parent_id.work_email)) # appending manager addresses
							# print to_emails
							# print table_data
							to_emails = list(set(to_emails))
							if to_emails:
								for email in to_emails:
									email_template_obj = self.pool.get('email.template')
									values = email_template_obj.generate_email_batch(cr, uid, template.id, [uid], context=context)
									values['subject'] = 'Reminder to Renew Tenancy Contracts' 
									values['email_to'] = email
									values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following tenancy documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>"%table_data
									values['body'] = 'body_html',
									values['res_id'] = False
									mail_mail_obj = self.pool.get('mail.mail')
									x = uid
									values[str(x)] = values.pop(uid)
									msg_id = mail_mail_obj.create(cr,uid,values)
									# if msg_id:
									mail_mail_obj.send(cr, uid, [msg_id], context=context) #To Send mail
			except Exception as e:
				pass
		return True


Tenancy_Company()

class Tenancy_Category(osv.osv):
	_name= 'tenancy.category'
	_rec_name = 'Category_name'
	_columns = {
		'Category_name' : fields.char('Category Name'),
	}
Tenancy_Category()


class Upload_tenancy_docs(osv.osv):
	_name='upload.tenancy.docs'
	_columns = {
		'tenancy_doc_id' : fields.many2one('tenancy.company'),
		'upload_image'  : fields.binary('Upload Doc(Image)'),
		'upload_pdf' : fields.binary('Upload Doc(Pdf)'),
	}
Upload_tenancy_docs()


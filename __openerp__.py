# description about module 

{
    "name": "Document Management",
    "author": "ehapi Technologies",
    "version": "1.0",
    'author': 'ehapi',
    'description': """This Module About Document Management Application
		1.Companies 
		2.Permits 
		3.Employess
		4.Reminders
		5.Reports
	""", 
	"category" : "Customization",
	"depends" : ["hr",'base','web_dashboard_tile','mail','doc_pro'],
	"demo_xml" : [], 
	"data" : [
					'security/security.xml',
					"security/ir.model.access.csv",
					"views/hr_ehapi_custom_view.xml",
					'views/permit_views.xml',
					 "views/custom_menu_view.xml",
					 'views/Tenancy_Contract_view.xml',
					 'views/accommodation_view.xml',
					 "views/document_report_view.xml",
					 "views/dashboard_view.xml",
					 "views/hr_training_view.xml",
					 "views/hr_recruitment_view.xml",
					 # "views/doc_notification_view.xml",
					 "edi/mail_data.xml",
					 "report/employee_details_report.xml",
					 'report/emp_details_report_view.xml',
					 "report/permits_report.xml",
					 'report/permits_report_view.xml',
					 'scheduler/schedulers.xml',
					 'views/insurance_policy_view.xml',
					 'views/supplier_contracts_view.xml',
					 # 'views/dubai_custom_view.xml',
					 # 'views/dafza_access_view.xml',
					 # 'views/pro_cards_view.xml',
					 # 'views/dwc_access_view.xml',
					 # 'views/dwc_contractor_access_view.xml',
					 # 'views/visa_cancellations_view.xml',
					 # 'views/insurance_certificates_view.xml',
					 ],
	'auto_install': False,
    'application': True, 
	"installable": True,
}

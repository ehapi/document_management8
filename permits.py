
from openerp.osv import fields, osv
import time
import openerp
from openerp.tools.translate import _
from openerp import api
import datetime
from datetime import timedelta
# import HTML
# import xlrd
import os


class Permits_Company(osv.osv):

    def seq_id(self, cr, uid, ids, context=None):
        myobj = self.pool.get('permits.company')
        ids = myobj.search(cr, uid, [])
        pid = len(ids) + 1
        sid = str(pid)
        return sid

    def permit_seq_id(self, cr, uid, ids, context=None):
        myobj = self.pool.get('permits.company')
        ids = myobj.search(cr, uid, [])
        pid = len(ids) + 1
        pid = 'PRMT00' + str(pid)
        return pid

    _name = "permits.company"
    _rec_name = 'name_permit'
    _columns = {
        's_no': fields.char('SL', readonly=True),
        'permit_no': fields.char('Reference No.'),
        'name_permit': fields.char('Permit / License / Contract Name', required=True),
        'authority': fields.many2one('permit.authority', 'Local Government Agency / Vendor Name '),
        'emirates': fields.many2one('emirates.name', 'Location', required=True),
        'tags': fields.many2many('permit.tags', 'permit_tags_rel', 'permit_id', 'permit_company_id', 'Tags'),
        'category': fields.many2one('permit.category', 'Category'),
        'start_date': fields.date('Issue Date'),
        'end_date': fields.date('Expiry Date'),
        'permit_incharge': fields.many2one('hr.employee', 'Incharge 1'),
        'permit_manager': fields.many2one('hr.employee', 'Incharge 2'),
        'fees': fields.char('Fees(AED)'),
        'first_reminder': fields.many2one('reminder.time', 'First Reminder(Days)', placeholder='20'),
        'company_id': fields.many2one('res.company', 'Company'),
        'second_reminder': fields.many2one('reminder.time', 'Second Reminder(Days)'),
        'status': fields.selection([('active', 'Active'), ('np1', 'Notification Period1'), ('np2', 'Notification Period2'), ('gp', 'Grace Period'), ('ur', 'Under Renewal'), ('rd', 'Renewed'), ('ex', 'Expired')], 'Permit Status', required=True),
        'permit_docs_id': fields.one2many('permit.upload.docs', 'permit_doc_id', 'Upload Docs'),
        'frequency_renewal_id': fields.many2one('frequency.renewal', 'Frequency of Renewal'),
        'remarks': fields.text('Remarks'),

    }
    _defaults = {
        's_no': seq_id,
        # 'permit_no' : permit_seq_id,
        'status': 'active'
    }

    @api.onchange('end_date')
    def _onchange_permitexpirydate(self):
        if self.start_date and self.end_date:
            start_date = datetime.datetime.strptime(
                self.start_date, '%Y-%m-%d')
            end_date = datetime.datetime.strptime(self.end_date, '%Y-%m-%d')
            if start_date > end_date:
                raise osv.except_osv(_("Error!"), _(
                    "Permit Start Date Must be less than Permit expiry Date"))
                return False

    @api.model
    def create(self, vals):
        if 'end_date' in vals:
            print("end date", vals['end_date'])
            try:
                start_date = datetime.datetime.strptime(
                    vals['start_date'], '%Y-%m-%d')
                end_date = datetime.datetime.strptime(
                    vals['end_date'], '%Y-%m-%d')
                if start_date > end_date:
                    raise osv.except_osv(_("Error!"), _(
                        "Permit Start Date Must be less than Permit expiry Date"))
                    return None
            except Exception, e:
                print("date exception", e)
        new_id = super(Permits_Company, self).create(vals)
        return new_id

    @api.multi
    def write(self, vals):
        if 'medicalend' in vals:
            start_date = datetime.datetime.strptime(
                self.start_date, '%Y-%m-%d')
            end_date = datetime.datetime.strptime(
                vals['start_date'], '%Y-%m-%d')
            if start_date > end_date:
                raise osv.except_osv(_("Error!"), _(
                    "Permit Start Date Must be less than Permit expiry Date"))
                return None

        new_id = super(Permits_Company, self).write(vals)
        return new_id

    @api.onchange('start_date')
    def _check_permit_end_date(self):
        if self.start_date:
            dateval = datetime.datetime.strptime(self.start_date, '%Y-%m-%d')
            self.end_date = dateval + timedelta(days=365)

    @api.onchange('end_date')
    def _check_permit_status(self):
        if self.end_date:
            dateval = datetime.datetime.strptime(self.end_date, '%Y-%m-%d')
            days_gap = (dateval - datetime.datetime.today()).days
            # print days_gap
            if days_gap >= 0:
                if 30 <= days_gap <= 35:
                    self.status = 'np1'

                elif 20 <= days_gap <= 30:
                    self.status = 'np2'

                elif 0 <= days_gap < 20:
                    self.status = 'gp'

                else:
                    self.status = 'active'
            else:
                self.status = 'ex'

    def notify_permits_incharge(self, cr, uid, context=None):
        template = self.pool.get('ir.model.data').get_object(
            cr, uid, 'document_management8', 'notify_permits_incharge')

        exp_date_limit = (datetime.datetime.today() +
                          timedelta(days=15)).date()
        # Getting permit Expiry docs
        permit_ids = self.pool.get('permits.company').search(cr, uid, [(
            'end_date', '>=', datetime.datetime.today()), ('end_date', '<=', exp_date_limit)])

        # expired permits
        expired_permits = self.pool.get('permits.company').search(cr, uid, [(
            'end_date', '<', datetime.datetime.today())])

        if expired_permits:
            self.pool.get('permits.company').write(cr, uid, expired_permits, {
                'status': 'ex'})  # moving into expired status

        managers = []
        incharge_data = self.pool.get('permits.company').read(
            cr, uid, permit_ids)
        incharges = []
        incharges2 = []
        for i in incharge_data:
            to_emails = []
            permits = []
            table_data = HTML.Table(
                header_row=['Document Reference', 'Permit Name', 'Expiry Date'])
            try:
                if 'permit_incharge' in i:  # Checking whether incharge is there or not
                    # Restricting multiple time notifications
                    if i['permit_incharge']:
                        if i['permit_incharge'][0] not in incharges:
                            incharges.append(i['permit_incharge'][0])
                            incharge_wise_permits = self.pool.get('permits.company').search(cr, uid, [(
                                'permit_incharge', '=', i['permit_incharge'][0]), ('id', 'in', permit_ids)])
                            incharge_wise_permits = list(set(incharge_wise_permits))
                            # print incharge_wise_permits
                            for j in self.pool.get('permits.company').browse(cr, uid, incharge_wise_permits):
                                table_data.rows.append(
                                    [str(j.permit_no), str(j.name_permit), str(j.end_date)])  # summarizing data
                                # appending to addresses
                                if j.permit_incharge.work_email:
                                    to_emails.append(j.permit_incharge.work_email)
                                # if j.permit_manager.work_email:
                                #     to_emails.append(j.permit_manager.work_email)
                                # to_emails.append(str(i.permit_incharge.parent_id.work_email))
                                # # appending manager addresses
                            to_emails = list(set(to_emails))
                            # print to_emails
                            if to_emails:
                                # for i in to_emails:
                                email_template_obj = self.pool.get(
                                    'email.template')
                                values = email_template_obj.generate_email_batch(
                                    cr, uid, template.id, [uid], context=context)
                                values['subject'] = 'Reminder to Renew Pemits'
                                values['email_to'] = to_emails[0]
                                values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following permit documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>" % table_data
                                values['body'] = 'body_html',
                                values['res_id'] = False
                                mail_mail_obj = self.pool.get('mail.mail')
                                x = uid
                                values[str(x)] = values.pop(uid)
                                msg_id = mail_mail_obj.create(cr, uid, values)
                                # if msg_id:
                                mail_mail_obj.send(
                                cr, uid, [msg_id], context=context)  # To Send
                                # mail
            except Exception as e:
                print e
                pass

        # Incharge 2

        for i in incharge_data:
            to_emails = []
            permits = []
            table_data = HTML.Table(
                header_row=['Document Reference', 'Permit Name', 'Expiry Date'])
            try:
                if 'permit_manager' in i:  # Checking whether incharge is there or not
                    # Restricting multiple time notifications
                    if i['permit_manager']:
                        if i['permit_manager'][0] not in incharges2:
                            incharges2.append(i['permit_manager'][0])
                            # incharges.append(i['permit_manager'][0])
                            incharge_wise_permits = self.pool.get('permits.company').search(cr, uid, [(
                                'permit_manager', '=', i['permit_manager'][0]), ('id', 'in', permit_ids)])
                            for j in self.pool.get('permits.company').browse(cr, uid, incharge_wise_permits):
                                table_data.rows.append(
                                    [str(j.permit_no), str(j.name_permit), str(j.end_date)])  # summarizing data
                                # appending to addresses
                                if j.permit_manager.work_email:
                                    to_emails.append(j.permit_manager.work_email)
                                # to_emails.append(str(i.permit_incharge.parent_id.work_email))
                                # # appending manager addresses
                            to_emails = list(set(to_emails))
                            if to_emails:
                                # for i in to_emails:
                                email_template_obj = self.pool.get(
                                    'email.template')
                                values = email_template_obj.generate_email_batch(
                                    cr, uid, template.id, [uid], context=context)
                                values['subject'] = 'Reminder to Renew Pemits'
                                values['email_to'] = to_emails[0]
                                values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following permit documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>" % table_data
                                values['body'] = 'body_html',
                                values['res_id'] = False
                                mail_mail_obj = self.pool.get('mail.mail')
                                x = uid
                                values[str(x)] = values.pop(uid)
                                msg_id = mail_mail_obj.create(cr, uid, values)
                                # if msg_id:
                                mail_mail_obj.send(
                                cr, uid, [msg_id], context=context)  # To Send
                                # mail
            except Exception as e:
                print e
                pass
        # incharge_ids = []
        # for i in incharge_data:
        #     if i.permit_incharge:
        #         incharge_ids.append(i.permit_incharge.id)
        #     if i.permit_manager:
        #         incharge_ids.append(i.permit_manager.id)
        # incharge_ids = list(set(incharge_ids))
        # pm_ids =  self.search(cr, uid, [('permit_manager','in', incharge_ids),('id', 'in', permit_ids)])
        # print pm_ids
        # incharge_wise_permits = self.pool.get('permits.company').browse(cr, uid, pm_ids)

        # permit_group = self.pool.get('res.groups').search(cr, uid, [('name', '=', 'Permits Incharge')])
        # users = self.pool.get('res.users').search(cr, uid, [('groups_id', 'in', permit_group)])
        # incharges = self.pool.get('hr.employee').search(cr, uid, [('user_id', 'in', users)])
        # incharges_data = self.pool.get('permits.company').read(
        #     cr, uid, permit_ids,['permit_incharge','permit_manager'])
        # incharges_ids = []
        # for i in incharges_data:
        #     # print i
        #     incharges_ids.append(i['permit_incharge'][0])
        #     incharges_ids.append(i['permit_manager'][0])

        # incharges_ids = list(set(incharges_ids))
        # print incharges_ids
        # to_emails = []
        # for i in incharges_ids:
        #     table_data = HTML.Table(
        #         header_row=['Document Reference', 'Expiry Date'])
        #     incharge_wise_permits = self.search(cr, uid, ['|',('permit_incharge','=', i),('permit_manager','=', i),('id','in',permit_ids)])
        #     for j in self.pool.get('permits.company').browse(cr, uid, incharge_wise_permits):
        #         table_data.rows.append(
        #             [str(j.permit_no), str(j.end_date)])  # summarizing data
        #         # appending to addresses
        #         if j.permit_manager.work_email:
        #             to_emails.append(j.permit_manager.work_email)
        #         # to_emails.append(str(i.permit_incharge.parent_id.work_email))
        #         # # appending manager addresses
        #     to_emails = list(set(to_emails))
        #     print to_emails
        #     if to_emails:
        #         # for i in to_emails:
        #         email_template_obj = self.pool.get(
        #             'email.template')
        #         values = email_template_obj.generate_email_batch(
        #             cr, uid, template.id, [uid], context=context)
        #         values['subject'] = 'Reminder to Renew Pemits'
        #         values['email_to'] = to_emails[0]
        #         values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following permit documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>" % table_data
        #         values['body'] = 'body_html',
        #         values['res_id'] = False
        #         mail_mail_obj = self.pool.get('mail.mail')
        #         x = uid
        #         values[str(x)] = values.pop(uid)
        #         msg_id = mail_mail_obj.create(cr, uid, values)
        #         # if msg_id:
        #         mail_mail_obj.send(
        #         cr, uid, [msg_id], context=context)  # To Send
        #         # mail
        return True

    @api.cr_uid_ids_context
    def get_doc_notification(self, cr, uid, context=None):
        notification_data = {}
        emp = self.pool.get('hr.employee').search(
            cr, uid, [('user_id', '=', uid)])
        contract_ids = self.search(
            cr, uid, [('status', '=', 'np1'), ('permit_incharge', '=', emp[0])])
        tenancy_ids = self.pool.get('tenancy.company').search(
            cr, uid, [('status', '=', 'np1'), ('tenancy_incharge', '=', emp[0])])
        if contract_ids:
            notification_data[1] = {
                'id': 1,
                'email_from': 'admin@cemex.com',
                'subject': 'Documents need to be renew',
                'company_id': False
            }
            # return notification_data or {}
        return notification_data

    def import_permits(self, cr, uid, ids, context=None):
        current_path = os.path.dirname(os.path.abspath(__file__))
        book = xlrd.open_workbook(
            current_path + '/excels/2. DATA_Permits & Licences & Contracts.xlsx')  # reading data
        sheet = book.sheet_by_index(0)
        for r in range(1, sheet.nrows):
            permit_dict = {}
            company = str(sheet.cell(r, 0).value).split('.')[0]

            company_id = self.pool.get('res.company').search(
                cr, uid, [('name', 'like', str(company))])
            if company_id:
                company = company_id[0]
            else:
                company = None

            location = str(sheet.cell(r, 1).value).rstrip()

            location_id = self.pool.get('emirates.name').search(
                cr, uid, [('name', '=ilike', str(location))])
            if not location_id:
                new_location = self.pool.get('emirates.name').create(
                    cr, uid, {'name': str(location)})
                location = new_location
            else:
                location = location_id[0]

            contract_name = str(sheet.cell(r, 2).value).rstrip()
            reference_no = str(sheet.cell(r, 3).value).rstrip('.0')
            authority = str(sheet.cell(r, 4).value).rstrip()

            authority_id = self.pool.get('permit.authority').search(
                cr, uid, [('authority_name', '=ilike', str(authority))])
            if len(authority_id) == 0:
                authority_new = self.pool.get('permit.authority').create(
                    cr, uid, {'authority_name': str(authority)})
                authority = authority_new
            else:
                authority = authority_id[0]

            issue_date = sheet.cell(r, 5).value
            try:
                issue_date = xlrd.xldate.xldate_as_datetime(
                    issue_date, book.datemode)
                issue_date = issue_date.date()
            except Exception:
                issue_date = None

            expiry = sheet.cell(r, 6).value
            try:
                expiry = xlrd.xldate.xldate_as_datetime(expiry, book.datemode)
                expiry = expiry.date()
            except Exception:
                expiry = None

            if issue_date and expiry:
                days_gap = (expiry - datetime.datetime.today().date()).days

                if days_gap >= 0:
                    if 30 <= days_gap <= 35:
                        permit_dict.update({"status": 'np1'})

                    elif 20 <= days_gap <= 30:
                        permit_dict.update({"status": 'np2'})

                    elif 0 <= days_gap < 20:
                        permit_dict.update({"status": 'gp'})

                    else:
                        permit_dict.update({"status": 'active'})
                else:
                    permit_dict.update({"status": 'ex'})
            responsible = sheet.cell(r, 7).value

            if responsible:
                emp_id = self.pool.get('hr.employee').search(
                    cr, uid, [('name', '=', str(responsible))])

                if emp_id:
                    emp_id = emp_id[0]

            responsible2 = sheet.cell(r, 8).value

            if responsible2:
                emp_id2 = self.pool.get('hr.employee').search(
                    cr, uid, [('name', '=', str(responsible2))])

                if emp_id2:
                    emp_id2 = emp_id2[0]

            fee = str(sheet.cell(r, 9).value).rstrip()
            ren_freq = sheet.cell(r, 10).value
            freq_ren_id = self.pool.get('frequency.renewal').search(
                cr, uid, [('period', '=', str(ren_freq))])
            if not freq_ren_id:
                freq_ren_id_new = self.pool.get('frequency.renewal').create(
                    cr, uid, {'period': str(ren_freq)})
                frequency_renewal_id = freq_ren_id_new
            else:
                frequency_renewal_id = freq_ren_id[0]
            remarks = str(sheet.cell(r, 11).value)

            permit_dict.update({
                'company_id': company,
                'emirates': location,
                'name_permit': contract_name,
                'permit_no': str(reference_no),
                'authority': authority,
                'start_date': issue_date,
                'end_date': expiry,
                'fees': fee,
                'frequency_renewal_id': frequency_renewal_id,
                'remarks': remarks,
                'permit_incharge': emp_id,
                'permit_manager': emp_id2,

            })

            # print permit_dict

            permit_id = self.search(
                cr, uid, [('permit_no', '=', str(reference_no))])

            if not permit_id:
                self.create(cr, uid, permit_dict)
            else:
                self.write(cr, uid, permit_id, permit_dict)

        return True

Permits_Company()


class Permit_Authority(osv.osv):
    _name = 'permit.authority'
    _rec_name = 'authority_name'
    _columns = {
        'authority_name': fields.char('Authority Name', required=True),
        'location': fields.char('Location')
    }

Permit_Authority()


class Emirates_name(osv.osv):
    _name = 'emirates.name'
    _rec_name = 'name'
    _columns = {
        'name': fields.char('Location Name', required=True),
        # 'code' : fields.char('Code')
    }
Emirates_name()


class Permit_tags(osv.osv):
    _name = 'permit.tags'
    _rec_name = 'name'
    _columns = {
        'permit_company_id': fields.many2many('permits.company'),
        'name': fields.char('Tag Name', required=True)
    }
Permit_tags()


class Permit_category(osv.osv):
    _name = 'permit.category'
    _rec_name = 'name'
    _columns = {
        'name': fields.char('Category Name')
    }
Permit_category()


class Reminder_time(osv.osv):
    _name = 'reminder.time'
    _rec_name = 'duration'
    _columns = {
        'duration': fields.integer('Duration(in days)', required=True),
    }
Reminder_time()


class Upload_Permit_Docs(osv.osv):
    _name = "permit.upload.docs"
    _columns = {
        'permit_doc_id': fields.many2one('permits.company'),
        'upload_image': fields.binary('Upload Doc(Image)'),
        'upload_pdf': fields.binary('Upload Doc(Pdf)'),
    }
Upload_Permit_Docs()


class Frequency_Renewal(osv.osv):
    _name = "frequency.renewal"
    _rec_name = 'period'
    _columns = {
        'period': fields.char('Renewal Period', required=True)
    }
Frequency_Renewal()

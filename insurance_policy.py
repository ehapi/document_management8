
from openerp.osv import fields, osv
import time
import openerp
from openerp.tools.translate import _
from openerp import api
import datetime
from datetime import timedelta
# import HTML
from datetime import datetime
import os
# import xlrd

class Insurance_policy(osv.osv):

    def seq_id(self, cr, uid, ids, context=None):
        myobj = self.pool.get('insurance.policy')
        ids = myobj.search(cr, uid, [])
        pid = len(ids) + 1
        sid = str(pid)
        return sid

    _name = "insurance.policy"
    _rec_name = 'insurance_policy_name'
    _columns = {
        's_no': fields.char('SL', readonly=True),
        'company_id': fields.many2one("res.company", "Company"),
        'insurance_policy_name': fields.char('Insurance Policy Name', required=True),
        'renwal_date': fields.date('Start Date', required=True),
        'expiry_date': fields.date('Expiry Date'),
        'incharge': fields.many2one('hr.employee', 'Incharge'),
        'insurance_manager': fields.many2one('hr.employee', 'Manager'),
        'first_reminder': fields.many2one('reminder.time', 'First Reminder(Days)', placeholder='15'),
        'second_reminder': fields.many2one('reminder.time', 'Second Reminder(Days)', placeholder='20'),
        'status': fields.selection([('active', 'Active'), ('np1', 'Notification Period1'), ('np2', 'Notification Period2'), ('gp', 'Grace Period'), ('ur', 'Under Renewal'), ('rd', 'Renewed'), ('ex', 'Expired')], 'Status'),

    }

    _defaults = {
        's_no': seq_id,
        'status': 'active',
    }

    def importing_xsl_file_data(self, cr, uid, ids, context=None):
        insurance_object = self.pool.get('insurance.policy')
        vals = {}
        current_path = os.path.dirname(os.path.abspath(__file__))
        xl_workbook = xlrd.open_workbook(
            current_path + '/excels/9. DATA_Insurance policies.xlsx')
        sheet_names = xl_workbook.sheet_names()
        xl_sheet = xl_workbook.sheet_by_name(sheet_names[0])
        for row in range(1, xl_sheet.nrows):
            company_id = xl_sheet.cell(row, 0).value
            if company_id:
                company_idd = self.pool.get('res.company').search(
                    cr, uid, [('name', 'like', str(company_id))])
                if company_idd:
                    company_idd = company_idd[0]

            insurance_policy_name = xl_sheet.cell(row, 1).value

            renwaldate = xl_sheet.cell(row, 2).value
            renwal_date = xlrd.xldate.xldate_as_datetime(
                renwaldate, xl_workbook.datemode)

            expirydate = xl_sheet.cell(row, 3).value
            expiry_date = xlrd.xldate.xldate_as_datetime(
                expirydate, xl_workbook.datemode)
            expiry_date = expiry_date.date()

            incharge = xl_sheet.cell(row, 4).value

            if incharge:
                emp_id = self.pool.get('hr.employee').search(cr, uid, [('name','=',str(incharge))])
                
                if emp_id:
                    emp_id = emp_id[0]

            vals = {
                'company_id': company_idd,
                'insurance_policy_name': insurance_policy_name,
                'renwal_date': renwal_date,
                'expiry_date': expiry_date,
                'incharge': emp_id,
            }

            if expiry_date:
                days_gap = (expiry_date - datetime.today().date()).days

                if days_gap >= 0:
                    if 30 <= days_gap <= 35:
                        vals.update({"status": 'np1'})

                    elif 20 <= days_gap <= 30:
                        vals.update({"status": 'np2'})

                    elif 0 <= days_gap < 20:
                        vals.update({"status": 'gp'})

                    else:
                        vals.update({"status": 'active'})
                else:
                    vals.update({"status": 'ex'})
            
            insurance_object.create(cr, uid, vals, context=context)

        return True

    def notify_medical_incharge(self, cr, uid, context=None):
        template = self.pool.get('ir.model.data').get_object(
            cr, uid, 'document_management8', 'notify_employee_incharge')
        exp_date_limit = (datetime.today() + timedelta(days=15)).date()
        # Checking Medical Docs
        medical_docs = self.search(cr, uid, [(
            'expiry_date', '>=', datetime.today()), ('expiry_date', '<=', exp_date_limit)])

        # expired medical docs
        expired_medicals = self.search(cr, uid, [(
            'expiry_date', '<', datetime.today())])

        if expired_medicals:
            # moving into expired status
            self.write(cr, uid, expired_medicals, {'status': 'ex'})

        managers = []
        incharge_data = self.read(cr, uid, medical_docs, ['incharge'])
        incharges = []
        for i in incharge_data:
            to_emails = []
            table_data = HTML.Table(
                header_row=['Document Reference', 'Expiry Date'])
            try:
                if 'incharge' in i:  # Checking whether incharge is there or not
                    if i['incharge'][0] not in incharges:  # Restricting multiple time notifications
                        incharges.append(i['incharge'][0])
                        incharge_wise_docs = self.search(
                            cr, uid, [('incharge', '=', i['incharge'][0]), ('id', 'in', medical_docs)])
                        for j in self.browse(cr, uid, incharge_wise_docs):
                            table_data.rows.append(
                                [str(j.insurance_policy_name), str(j.expiry_date)])  # summarizing data
                            # appending to addresses
                            to_emails.append(str(j.incharge.work_email))
                            if j.insurance_manager:
                                to_emails.append(
                                    str(j.insurance_manager.work_email))
                            # to_emails.append(str(i.permit_incharge.parent_id.work_email))
                            # # appending manager addresses
                        to_emails = list(set(to_emails))
                        if to_emails:
                            for email in to_emails:
                                email_template_obj = self.pool.get(
                                    'email.template')
                                values = email_template_obj.generate_email_batch(
                                    cr, uid, template.id, [uid], context=context)
                                values[
                                    'subject'] = 'Reminder to Renew Insurance Policy Documents'
                                values['email_to'] = email
                                values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following insurance policy documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>" % table_data
                                values['body'] = 'body_html',
                                values['res_id'] = False
                                mail_mail_obj = self.pool.get('mail.mail')
                                x = uid
                                values[str(x)] = values.pop(uid)
                                msg_id = mail_mail_obj.create(cr, uid, values)
                                # if msg_id:
                                mail_mail_obj.send(
                                    cr, uid, [msg_id], context=context)  # To Send mail
            except Exception as e:
                pass
        # print to_emails
        return True

    @api.onchange('expiry_date')
    def _check_permit_status(self):
        if self.expiry_date:
            dateval = datetime.strptime(self.expiry_date, '%Y-%m-%d')
            days_gap = (dateval - datetime.today()).days
            if days_gap >= 0:
                if 30 <= days_gap <= 35:
                    self.status = 'np1'

                elif 20 <= days_gap <= 30:
                    self.status = 'np2'

                elif 0 <= days_gap < 20:
                    self.status = 'gp'

                else:
                    self.status = 'active'
            else:
                self.status = 'ex'
Insurance_policy()

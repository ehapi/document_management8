
from openerp.osv import fields, osv
import time
import openerp
from openerp.tools.translate import _
from openerp import api
import datetime
from datetime import timedelta
from datetime import datetime
# import xlrd
import os
# import HTML


class Supplier_Contracts(osv.osv):

    def seq_id(self, cr, uid, ids, context=None):
        myobj = self.pool.get('supplier.contracts')
        ids = myobj.search(cr, uid, [])
        pid = len(ids) + 1
        sid = str(pid)
        return sid
    _name = "supplier.contracts"
    _rec_name = 'supplier_category_id'
    _columns = {
        's_no': fields.char('SL', readonly=True),
        'company_id': fields.many2one("res.company", 'Company'),
        'location': fields.many2one('supplier.location', 'Location'),
        'supplier_category_id': fields.many2one('supplier.category', 'Category'),
        'details': fields.text('Details'),
        'start_date': fields.date('Start'),
        'end_date': fields.date('End'),
        'end_user': fields.many2one('hr.employee', 'Enduser'),
        'supervisor': fields.many2one('hr.employee', 'Supervisor'),
        'responsible': fields.many2one('hr.employee', 'Responsible'),
        'responsible_2': fields.many2one('hr.employee', 'Responsible 2'),
        'no_of_service': fields.char('No of service'),
        'renewal_frequency': fields.many2one('reminder.time', 'Renewal Frequency'),
        'contract_docs_id': fields.one2many('supplier.contract.docs', 'contract_doc_id', 'Upload Docs'),
        'status': fields.selection([('active', 'Active'), ('np1', 'Notification Period1'), ('np2', 'Notification Period2'), ('gp', 'Grace Period'), ('ur', 'Under Renewal'), ('rd', 'Renewed'), ('ex', 'Expired')], 'Status', required=True),


    }
    _defaults = {
        's_no': seq_id,
        'status': 'active',
    }

    def import_supplier_contracts(self, cr, uid, ids, context=None):

        contracts_obj = self.pool.get('supplier.contracts')
        current_path = os.path.dirname(os.path.abspath(__file__))
        book = xlrd.open_workbook(
            current_path + '/excels/8. DATA_ Supplier Contracts Inventory 2016.xlsx')  # reading data
        sheet = book.sheet_by_index(0)
        for r in range(4, sheet.nrows):
            company_id = str(sheet.cell(r, 0).value).rstrip()
            if company_id:
                company_idd = self.pool.get('res.company').search(
                    cr, uid, [('name', 'ilike', str(company_id))])
                if company_idd:
                    company_idd = company_idd[0]
                else:
                    company_idd = None

            location = str(sheet.cell(r, 1).value).rstrip()
            if location:
                location_id = self.pool.get('supplier.location').search(
                    cr, uid, [('name', '=like', str(location))])
                if location_id:
                    location_id = location_id[0]
                else:
                    location_id = self.pool.get('supplier.location').create(
                        cr, uid, {'name': str(location)})

            supplier_category_id = str(sheet.cell(r, 2).value).rstrip()
            if supplier_category_id:
                supplier_category_idd = self.pool.get('supplier.category').search(
                    cr, uid, [('name', '=like', str(supplier_category_id))])
                if supplier_category_idd:
                    supplier_category_idd = supplier_category_idd[0]
                else:
                    supplier_category_idd = self.pool.get('supplier.category').create(
                        cr, uid, {'name': str(supplier_category_id)})

            details = str(sheet.cell(r, 3).value).rstrip()
            start_date = sheet.cell(r, 4).value
            if start_date:
                start_date = xlrd.xldate.xldate_as_datetime(
                    start_date, book.datemode)
                start_date.date()
            else:
                start_date = None
            end_date = sheet.cell(r, 5).value

            if end_date:
                end_date = xlrd.xldate.xldate_as_datetime(
                    end_date, book.datemode)
                end_date = end_date.date()
            else:
                end_date = None
            end_user = str(sheet.cell(r, 6).value).rstrip()
            if end_user:
                end_user_id = self.pool.get('hr.employee').search(
                    cr, uid, [('name', '=ilike', str(end_user))])
                if end_user_id:
                    end_user_id = end_user_id[0]

            supervisor = str(sheet.cell(r, 7).value).rstrip()
            if supervisor:
                supervisor_id = self.pool.get('hr.employee').search(
                    cr, uid, [('name', '=ilike', str(supervisor))])
                if supervisor_id:
                    supervisor_id = supervisor_id[0]

            responsible = str(sheet.cell(r, 8).value).rstrip()
            
            if responsible:
                responsible_id = self.pool.get('hr.employee').search(
                    cr, uid, [('name', '=', str(responsible))])
                if responsible_id:
                    responsible_id = responsible_id[0]

            no_of_service = sheet.cell(r, 9).value
            if no_of_service:
                no_of_service = int(no_of_service)
            renewal_frequency = str(sheet.cell(r, 10).value).rstrip('.0')
            if renewal_frequency:
                renewal_frequency_id = self.pool.get('reminder.time').search(
                    cr, uid, [('duration', '=', int(renewal_frequency))])
                if renewal_frequency_id:
                    renewal_frequency_id = renewal_frequency_id[0]
                else:
                    renewal_frequency_id = self.pool.get('reminder.time').create(
                        cr, uid, {'duration': int(renewal_frequency)})

            vals = {
                'company_id': company_idd,
                'location': location_id,
                'supplier_category_id': supplier_category_idd,
                'details': details,
                'start_date': start_date,
                'end_date': end_date,
                'end_user': end_user_id,
                'supervisor': supervisor_id,
                'responsible': responsible_id,
                'no_of_service': no_of_service,
                'renewal_frequency': renewal_frequency_id,
            }

            if end_date:
                days_gap = (end_date - datetime.today().date()).days

                if days_gap >= 0:
                    if 30 <= days_gap <= 35:
                        vals.update({"status": 'np1'})

                    elif 20 <= days_gap <= 30:
                        vals.update({"status": 'np2'})

                    elif 0 <= days_gap < 20:
                        vals.update({"status": 'gp'})

                    else:
                        vals.update({"status": 'active'})
                else:
                    vals.update({"status": 'ex'})

            cont_id = contracts_obj.search(
                cr, uid, [('details', '=', details)])

            if not cont_id:
                contracts_obj.create(cr, uid, vals, context=context)
            else:
                contracts_obj.write(cr, uid, cont_id, vals, context=context)
        return True

    @api.onchange('end_date')
    def _check_permit_status(self):
        if self.end_date:
            dateval = datetime.strptime(self.end_date, '%Y-%m-%d')
            days_gap = (dateval - datetime.today()).days
            if days_gap >= 0:
                if 30 <= days_gap <= 35:
                    self.status = 'np1'

                elif 20 <= days_gap <= 30:
                    self.status = 'np2'

                elif 0 <= days_gap < 20:
                    self.status = 'gp'

                else:
                    self.status = 'active'
            else:
                self.status = 'ex'

    def notify_supplier_contract_incharge(self, cr, uid, context=None):
        template = self.pool.get('ir.model.data').get_object(
            cr, uid, 'document_management8', 'notify_employee_incharge')
        exp_date_limit = (datetime.today() + timedelta(days=15)).date()
        # Checking Medical Docs
        contract_docs = self.search(cr, uid, [(
            'end_date', '>=', datetime.today()), ('end_date', '<=', exp_date_limit)])

        # expired contracts docs
        expired_contracts = self.search(cr, uid, [(
            'end_date', '<', datetime.today())])

        if expired_contracts:
            self.write(cr, uid, expired_contracts, {'status':'ex'}) # moving into expired status

        managers = []
        incharge_data = self.read(cr, uid, contract_docs, ['responsible'])
        incharges = []
        for i in incharge_data:
            to_emails = []
            table_data = HTML.Table(
                header_row=['Document Reference', 'Expiry Date'])
            try:
                if 'responsible' in i:  # Checking whether responsible is there or not
                    # Restricting multiple time notifications
                    if i['responsible'][0] not in incharges:
                        incharges.append(i['responsible'][0])
                        incharge_wise_docs = self.search(
                            cr, uid, [('responsible', '=', i['responsible'][0]), ('id', 'in', contract_docs)])
                        for j in self.browse(cr, uid, incharge_wise_docs):
                            table_data.rows.append(
                                [str(j.details), str(j.end_date)])  # summarizing data
                            # appending to addresses
                            to_emails.append(str(j.responsible.work_email))
                            if i.responsible_2:
                                to_emails.append(str(j.responsible_2.work_email))
                            # to_emails.append(str(i.permit_incharge.parent_id.work_email))
                            # # appending manager addresses
                        to_emails = list(set(to_emails))
                        if to_emails:
                            for email in to_emails:
                                email_template_obj = self.pool.get(
                                    'email.template')
                                values = email_template_obj.generate_email_batch(
                                    cr, uid, template.id, [uid], context=context)
                                values[
                                    'subject'] = 'Reminder to Renew Supplier Contract Documents'
                                values['email_to'] = email
                                values['body_html'] = "Dear Sir/Madam,<br><br>This is to inform you that following supplier contract documents are going to expire on below mentioned dates. Kindly take a necessary action to avoid expiration.\n<br>%s\n<br><br>&nbsp;Thank You <br>&nbsp;Cemex Document PRO <br>" % table_data
                                values['body'] = 'body_html',
                                values['res_id'] = False
                                mail_mail_obj = self.pool.get('mail.mail')
                                x = uid
                                values[str(x)] = values.pop(uid)
                                msg_id = mail_mail_obj.create(cr, uid, values)
                                # if msg_id:
                                mail_mail_obj.send(
                                    cr, uid, [msg_id], context=context)  # To Send mail
            except Exception as e:
                pass
        # print to_emails
        return True

Supplier_Contracts()


class Upload_Supplier_Contract_Docs(osv.osv):
    _name = "supplier.contract.docs"
    _columns = {
        'contract_doc_id': fields.many2one('supplier.contracts'),
        'upload_image': fields.binary('Upload Doc(Image)'),
        'upload_pdf': fields.binary('Upload Doc(Pdf)'),
    }
Upload_Supplier_Contract_Docs()

class Supplier_Location(osv.osv):
    _name = 'supplier.location'
    _rec_name = 'name'
    _columns = {
        'name': fields.char('Location', required=True)
    }
Supplier_Location()


class Supplier_Category(osv.osv):
    _name = 'supplier.category'
    _rec_name = 'name'
    _columns = {
        'name': fields.char('Category Name', required=True)
    }
Supplier_Location()

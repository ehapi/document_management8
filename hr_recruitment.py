from openerp.osv import fields, osv
import time
import openerp
from openerp.tools.translate import _
from openerp import api
import datetime
from datetime import timedelta
# import HTML

class Hr_recruitement(osv.osv):
	def seq_id(self, cr, uid, ids, context=None):
		myobj = self.pool.get('hr.recruitment')
		ids = myobj.search(cr, uid, [])
		pid = len(ids) + 1
		sid = str(pid)
		return sid

	_name = "hr.recruitment"
	_rec_name = 'candidate_name'
	_columns = {
		's_no' : fields.char('SL',readonly=True),
		'company' :fields.many2one('res.company','Company'),
		'department_id' : fields.many2one('hr.department','Department'),
		'position_id' : fields.many2one('hr.job','Position'),
		'location_id' : fields.many2one('company.location','Location'),
		'emp_id' : fields.many2one('hr.employee','Employee ID'),
		'replacement' : fields.char('Replacement'),
		'category_id' : fields.many2one('recruitment.category','Category'),
		'mrf_date' : fields.date('MRF Date'),
		'date_closed' : fields.date('Date Closed'),
		'tat' : fields.char('TAT'),
		'quarter' : fields.char('Quarter'),
		'candidate_name' : fields.char('Candidate Name'),
		'total_days_taken_close' : fields.char('Total Days Taken to Close'),
		'balance_open_position' : fields.char('Balance Open Position'),
		'month' : fields.char('Month'),
		'category_id2' : fields.many2one('recruitment.category','Category'),
		'candidate_name2' : fields.char('Candidate Name'),
		'justification' : fields.char('Justification'),
		'recruited_by' : fields.many2one('hr.employee','Recruited By'),
		'action_required' : fields.char('Action Required'),
		'candidate_name3' : fields.char('Candidate Name'),
		'status_follow_up' : fields.text("Status And Follow Up Action"),
		'remarks' : fields.text('Remarks'),

		# 'completion_date' : fields.date('Completion Date'),
		# 'completion_type' : fields.selection([('month','Month'),('days','days')],'Completion'),
		# 'duration' : fields.char('Duration'),
		# 'department' : fields.many2one('hr.department',"Department"),
		# 'profession_id' : fields.many2one('hr.job',"Profession"),
		# 'category_id' : fields.many2one('training.category','Category'),
		# 'sub_category_id' : fields.many2one('sub.training.category','Sub Category'),
		# 'institution_id' : fields.many2one('hr.institution','Institution'),
	}

	_defaults = {
		's_no' : seq_id,
	}
Hr_recruitement()

class Company_Location(osv.osv):
	_name = 'company.location'
	_rec_name = 'location_name'
	_columns = {
		'location_name' : fields.char('Location Name',required=True)
	}
Company_Location()

class Recruitment_Category_main(osv.osv):
	_name = 'recruitment.category'
	_rec_name = 'category_name'
	_columns = {
		'category_name' : fields.char('Category Name',required=True)
	}
Recruitment_Category_main()


